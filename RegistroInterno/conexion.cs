﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OracleClient;
using System.Data;

namespace RegistroInterno
{
    class conexion
    {
        static private OracleConnection conn = new OracleConnection();

        static conexion()
        {
            conn.ConnectionString = "Data Source=XE;User ID=REGISTRO;Password=interno;Unicode=True;";

        }

        public int Comando(string strComando)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = strComando;
            cmd.CommandType = CommandType.Text;
            try
            {
                conn.Open();
                int res = cmd.ExecuteNonQuery();
                conn.Close();
                return res;
            }
            catch (Exception e)
            {
                conn.Close();
                return 0;
            }

        }

        public int Comando(string strComando, string[] campos, string[] parametros)
        {
            OracleCommand cmd = new OracleCommand(strComando, conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            for (int i = 0; i < campos.Length; i++)
            {
                cmd.Parameters.AddWithValue(campos[i], parametros[i]);
            }
            try
            {
                conn.Open();
                int res = cmd.ExecuteNonQuery();
                conn.Close();
                return res;
            }
            catch (Exception e)
            {
                conn.Close();
                return 0;
            }

        }

        public DataTable Llenartabla(string strProcedimiento,string strView)
        {
            conn.Open();
            OracleCommand comd = new OracleCommand(strProcedimiento, conn);
            comd.CommandType = CommandType.StoredProcedure;
            comd.Parameters.Add(strView, OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comd;
            DataTable tabla = new DataTable();
            adapter.Fill(tabla);
            conn.Close();
            return tabla;
        }

        public List<string> combobx(string strConsulta, string strCampo)
        {
            List<string> lista = new List<string>();
            OracleCommand command = new OracleCommand(strConsulta, conn);
            conn.Open();
            try
            {
                OracleDataReader registro = command.ExecuteReader();
                while (registro.Read())
                {
                    lista.Add(registro[strCampo].ToString());

                }
                conn.Close();
                return lista;
            }
            catch (Exception e)
            {
                conn.Close();
                return null;
            }
        }
    }
}
