﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroInterno
{
    public partial class frmNuevoEgreso : Form
    {
        conexion con = new conexion();

        public frmNuevoEgreso()
        {
            InitializeComponent();
        }

        private void clickCancelar(object sender, EventArgs e)
        {
            this.Hide();
            frmEgresos egresos = new frmEgresos("mostrarEgresos",4);
            egresos.Show();
        }

        private void mouseCancelarEnter(object sender, EventArgs e)
        {
            bttnCancelar.BackColor = Color.NavajoWhite;
        }

        private void mouseCancelarLeave(object sender, EventArgs e)
        {
            bttnCancelar.BackColor = Color.Orange;
        }

        private void mouseGuardarEnter(object sender, EventArgs e)
        {
            bttnGuardar.BackColor = Color.CornflowerBlue;
        }

        private void mouseGuardarLeave(object sender, EventArgs e)
        {
            bttnGuardar.BackColor = Color.RoyalBlue;
        }

        private void frmNuevoEgreso_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void bttnGuardar_Click(object sender, EventArgs e)
        {
            string[] campos = {
                "vNOMBRE", "vAP_PATERNO", "vAP_MATERNO", "vEDAD", "vSEXO", "vFECHA_NAC", "vESTADO_CIVIL", "vESCOLARIDAD"
                ,"vOCUPACION", "vID_INTERNO", "vTIPO", "vESTADO_GRAL"
            };

            string[] valores =
            {
                txtBxNombre.Text, txtBxApPaterno.Text, txtBxApMaterno.Text, numUpDnEdad.Value.ToString(), cmbBxSexo.Text, dtTmFechaNac.Text,
                cmbBxEstadoCivil.Text, cmbBxEscolaridad.Text, txtBxOcupacion.Text, 1.ToString(), txtBxTipoEgreso.Text, txtBxEstadoGral.Text
            };
            con.Comando("INSERTAREGRESOS", campos, valores);

            if (!valores.Contains(""))
            {
                MessageBox.Show("Egreso registrado exitosamente");
                this.Hide();
                frmEgresos egresos = new frmEgresos("mostrarEgresos", 4);
                egresos.Show();
            }
            else
            {
                MessageBox.Show("ALGO FALLÓ");
            }
        }
    }
}
