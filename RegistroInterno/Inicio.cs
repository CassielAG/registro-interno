﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RegistroInterno.Properties;

namespace RegistroInterno
{
    public partial class frmInicio : Form
    {
        string contrasenia = Settings.Default.Contrasenia;

        public frmInicio()
        {
            InitializeComponent();
        }

        private void mouseLeave(object sender, EventArgs e)
        {
            btnIngresar.BackColor = Color.Orange;
        }

        private void mousEnter(object sender, EventArgs e)
        {
            btnIngresar.BackColor = Color.NavajoWhite;
        }

        private void ingresarClick(object sender, EventArgs e)
        {
            if(txtBxContrasena.Text == contrasenia){
                this.Hide();
                frmMenu menu = new frmMenu();
                menu.Show();
            }else if(txtBxContrasena.Text == "")
            {
                MessageBox.Show("INGRESE UNA CONTRASEÑA POR FAVOR");
            }
            else{
                MessageBox.Show("CONTRASEÑA INCORRECTA");
            }
        }

        private void chckBxMostrar_CheckedChanged(object sender, EventArgs e)
        {
            
            if(!chckBxMostrar.Checked)
            {
                txtBxContrasena.UseSystemPasswordChar = true;
            }
            else
            {
                txtBxContrasena.UseSystemPasswordChar = false;
            }
                
        }
    }
}
