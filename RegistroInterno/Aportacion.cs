﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroInterno
{
    public partial class frmAportacion : Form
    {
        public frmAportacion()
        {
            InitializeComponent();
        }

        private void frmAportacion_Load(object sender, EventArgs e)
        {
            this.rprtVwAportacion.RefreshReport();
        }

        private void frmAportacion_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            frmInternos internos = new frmInternos("mostrarInternos",3);
            internos.Show();
        }
    }
}
