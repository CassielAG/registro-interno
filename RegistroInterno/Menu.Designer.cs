﻿namespace RegistroInterno
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblVizualizar = new System.Windows.Forms.Label();
            this.btnInternos = new System.Windows.Forms.Button();
            this.btnEgresos = new System.Windows.Forms.Button();
            this.linkLblCamContra = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // lblVizualizar
            // 
            this.lblVizualizar.AutoSize = true;
            this.lblVizualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVizualizar.Location = new System.Drawing.Point(75, 45);
            this.lblVizualizar.Name = "lblVizualizar";
            this.lblVizualizar.Size = new System.Drawing.Size(273, 29);
            this.lblVizualizar.TabIndex = 0;
            this.lblVizualizar.Text = "¿Qué deseas visualizar?";
            // 
            // btnInternos
            // 
            this.btnInternos.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnInternos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnInternos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInternos.ForeColor = System.Drawing.Color.White;
            this.btnInternos.Location = new System.Drawing.Point(126, 112);
            this.btnInternos.Margin = new System.Windows.Forms.Padding(0);
            this.btnInternos.Name = "btnInternos";
            this.btnInternos.Size = new System.Drawing.Size(151, 53);
            this.btnInternos.TabIndex = 1;
            this.btnInternos.Text = "Internos";
            this.btnInternos.UseVisualStyleBackColor = false;
            this.btnInternos.Click += new System.EventHandler(this.clickInternos);
            this.btnInternos.MouseEnter += new System.EventHandler(this.mouseEnterInt);
            this.btnInternos.MouseLeave += new System.EventHandler(this.mouseLeaveInt);
            // 
            // btnEgresos
            // 
            this.btnEgresos.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnEgresos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEgresos.ForeColor = System.Drawing.Color.White;
            this.btnEgresos.Location = new System.Drawing.Point(126, 195);
            this.btnEgresos.Name = "btnEgresos";
            this.btnEgresos.Size = new System.Drawing.Size(151, 58);
            this.btnEgresos.TabIndex = 2;
            this.btnEgresos.Text = "Egresos";
            this.btnEgresos.UseVisualStyleBackColor = false;
            this.btnEgresos.Click += new System.EventHandler(this.btnBendiciones_Click);
            this.btnEgresos.MouseEnter += new System.EventHandler(this.mouseEnterBen);
            this.btnEgresos.MouseLeave += new System.EventHandler(this.mouseLeaveBen);
            // 
            // linkLblCamContra
            // 
            this.linkLblCamContra.AutoSize = true;
            this.linkLblCamContra.Location = new System.Drawing.Point(152, 294);
            this.linkLblCamContra.Name = "linkLblCamContra";
            this.linkLblCamContra.Size = new System.Drawing.Size(101, 13);
            this.linkLblCamContra.TabIndex = 3;
            this.linkLblCamContra.TabStop = true;
            this.linkLblCamContra.Text = "Cambiar contraseña";
            this.linkLblCamContra.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.clickCambiarContrasena);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(403, 374);
            this.Controls.Add(this.linkLblCamContra);
            this.Controls.Add(this.btnEgresos);
            this.Controls.Add(this.btnInternos);
            this.Controls.Add(this.lblVizualizar);
            this.MaximizeBox = false;
            this.Name = "frmMenu";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMenu_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblVizualizar;
        private System.Windows.Forms.Button btnInternos;
        private System.Windows.Forms.Button btnEgresos;
        private System.Windows.Forms.LinkLabel linkLblCamContra;
    }
}