﻿namespace RegistroInterno
{
    partial class frmNuevoEgreso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblOcupacion = new System.Windows.Forms.Label();
            this.txtBxOcupacion = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblApMaterno = new System.Windows.Forms.Label();
            this.txtBxApMaterno = new System.Windows.Forms.TextBox();
            this.cmbBxEstadoCivil = new System.Windows.Forms.ComboBox();
            this.cmbBxEscolaridad = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbBxSexo = new System.Windows.Forms.ComboBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.numUpDnEdad = new System.Windows.Forms.NumericUpDown();
            this.txtBxNombre = new System.Windows.Forms.TextBox();
            this.lblEdad = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dtTmFechaNac = new System.Windows.Forms.DateTimePicker();
            this.lblApPaterno = new System.Windows.Forms.Label();
            this.lblFechaNac = new System.Windows.Forms.Label();
            this.txtBxApPaterno = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxCP = new System.Windows.Forms.TextBox();
            this.lblCP = new System.Windows.Forms.Label();
            this.txtBxEstado = new System.Windows.Forms.TextBox();
            this.lblEstado = new System.Windows.Forms.Label();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxMunicipio = new System.Windows.Forms.TextBox();
            this.lblMunicipio = new System.Windows.Forms.Label();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxColonia = new System.Windows.Forms.TextBox();
            this.lblColonia = new System.Windows.Forms.Label();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDom = new System.Windows.Forms.Label();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxNum = new System.Windows.Forms.TextBox();
            this.lblNum = new System.Windows.Forms.Label();
            this.txtBxCalle = new System.Windows.Forms.TextBox();
            this.lblCalle = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxCPFam = new System.Windows.Forms.TextBox();
            this.lblCPFam = new System.Windows.Forms.Label();
            this.txtBxEstadoFam = new System.Windows.Forms.TextBox();
            this.lblEstadoFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxMunicipioFam = new System.Windows.Forms.TextBox();
            this.lblMunicipioFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxColoniaFam = new System.Windows.Forms.TextBox();
            this.lblColoniaFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDomFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxNumFam = new System.Windows.Forms.TextBox();
            this.lblNumFam = new System.Windows.Forms.Label();
            this.txtBxCalleFam = new System.Windows.Forms.TextBox();
            this.lblCalleFam = new System.Windows.Forms.Label();
            this.bttnCancelar = new System.Windows.Forms.Button();
            this.bttnGuardar = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTipoEgreso = new System.Windows.Forms.Label();
            this.txtBxTipoEgreso = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.lblEstaGral = new System.Windows.Forms.Label();
            this.txtBxEstadoGral = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnEdad)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 572F));
            this.tableLayoutPanel4.Controls.Add(this.lblOcupacion, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtBxOcupacion, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(12, 158);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(672, 34);
            this.tableLayoutPanel4.TabIndex = 39;
            // 
            // lblOcupacion
            // 
            this.lblOcupacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOcupacion.AutoSize = true;
            this.lblOcupacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOcupacion.Location = new System.Drawing.Point(3, 8);
            this.lblOcupacion.Name = "lblOcupacion";
            this.lblOcupacion.Size = new System.Drawing.Size(94, 17);
            this.lblOcupacion.TabIndex = 14;
            this.lblOcupacion.Text = "Ocupación:";
            // 
            // txtBxOcupacion
            // 
            this.txtBxOcupacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxOcupacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxOcupacion.Location = new System.Drawing.Point(103, 3);
            this.txtBxOcupacion.MaxLength = 50;
            this.txtBxOcupacion.Multiline = true;
            this.txtBxOcupacion.Name = "txtBxOcupacion";
            this.txtBxOcupacion.Size = new System.Drawing.Size(529, 28);
            this.txtBxOcupacion.TabIndex = 15;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.68984F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.31016F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 146F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 156F));
            this.tableLayoutPanel3.Controls.Add(this.lblApMaterno, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtBxApMaterno, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmbBxEstadoCivil, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmbBxEscolaridad, 3, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(12, 118);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(672, 34);
            this.tableLayoutPanel3.TabIndex = 38;
            // 
            // lblApMaterno
            // 
            this.lblApMaterno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblApMaterno.AutoSize = true;
            this.lblApMaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApMaterno.Location = new System.Drawing.Point(3, 8);
            this.lblApMaterno.Name = "lblApMaterno";
            this.lblApMaterno.Size = new System.Drawing.Size(118, 17);
            this.lblApMaterno.TabIndex = 10;
            this.lblApMaterno.Text = "Apellido Materno:";
            // 
            // txtBxApMaterno
            // 
            this.txtBxApMaterno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxApMaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxApMaterno.Location = new System.Drawing.Point(127, 3);
            this.txtBxApMaterno.MaxLength = 20;
            this.txtBxApMaterno.Multiline = true;
            this.txtBxApMaterno.Name = "txtBxApMaterno";
            this.txtBxApMaterno.Size = new System.Drawing.Size(224, 28);
            this.txtBxApMaterno.TabIndex = 11;
            // 
            // cmbBxEstadoCivil
            // 
            this.cmbBxEstadoCivil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBxEstadoCivil.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBxEstadoCivil.FormattingEnabled = true;
            this.cmbBxEstadoCivil.Items.AddRange(new object[] {
            "Estado civil",
            "Casado",
            "Soltero"});
            this.cmbBxEstadoCivil.Location = new System.Drawing.Point(372, 4);
            this.cmbBxEstadoCivil.MaxDropDownItems = 3;
            this.cmbBxEstadoCivil.Name = "cmbBxEstadoCivil";
            this.cmbBxEstadoCivil.Size = new System.Drawing.Size(140, 24);
            this.cmbBxEstadoCivil.TabIndex = 12;
            this.cmbBxEstadoCivil.Text = "Estado civil";
            // 
            // cmbBxEscolaridad
            // 
            this.cmbBxEscolaridad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBxEscolaridad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBxEscolaridad.FormattingEnabled = true;
            this.cmbBxEscolaridad.Items.AddRange(new object[] {
            "Escolaridad",
            "Primaria",
            "Primaria trunca",
            "Secundaria",
            "Secundaria trunca",
            "Preparatoria",
            "Preparatoria trunca",
            "Licenciatura",
            "Maestria",
            "Doctorado"});
            this.cmbBxEscolaridad.Location = new System.Drawing.Point(518, 4);
            this.cmbBxEscolaridad.MaxDropDownItems = 3;
            this.cmbBxEscolaridad.Name = "cmbBxEscolaridad";
            this.cmbBxEscolaridad.Size = new System.Drawing.Size(151, 24);
            this.cmbBxEscolaridad.TabIndex = 13;
            this.cmbBxEscolaridad.Text = "Escolaridad";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 26);
            this.label1.TabIndex = 35;
            this.label1.Text = "Nuevo Egreso";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.83178F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.16822F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLayoutPanel1.Controls.Add(this.cmbBxSexo, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNombre, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.numUpDnEdad, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtBxNombre, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblEdad, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 38);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(672, 34);
            this.tableLayoutPanel1.TabIndex = 36;
            // 
            // cmbBxSexo
            // 
            this.cmbBxSexo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbBxSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBxSexo.FormattingEnabled = true;
            this.cmbBxSexo.Items.AddRange(new object[] {
            "Sexo",
            "Masculino",
            "Femenino"});
            this.cmbBxSexo.Location = new System.Drawing.Point(576, 4);
            this.cmbBxSexo.MaxDropDownItems = 3;
            this.cmbBxSexo.Name = "cmbBxSexo";
            this.cmbBxSexo.Size = new System.Drawing.Size(93, 24);
            this.cmbBxSexo.TabIndex = 6;
            this.cmbBxSexo.Text = "Sexo";
            // 
            // lblNombre
            // 
            this.lblNombre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(3, 8);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(96, 17);
            this.lblNombre.TabIndex = 2;
            this.lblNombre.Text = "Nombre (s):";
            // 
            // numUpDnEdad
            // 
            this.numUpDnEdad.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.numUpDnEdad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUpDnEdad.Location = new System.Drawing.Point(491, 5);
            this.numUpDnEdad.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numUpDnEdad.Name = "numUpDnEdad";
            this.numUpDnEdad.Size = new System.Drawing.Size(55, 23);
            this.numUpDnEdad.TabIndex = 4;
            this.numUpDnEdad.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // txtBxNombre
            // 
            this.txtBxNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxNombre.Location = new System.Drawing.Point(105, 3);
            this.txtBxNombre.MaxLength = 20;
            this.txtBxNombre.Multiline = true;
            this.txtBxNombre.Name = "txtBxNombre";
            this.txtBxNombre.Size = new System.Drawing.Size(304, 28);
            this.txtBxNombre.TabIndex = 1;
            // 
            // lblEdad
            // 
            this.lblEdad.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEdad.AutoSize = true;
            this.lblEdad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdad.Location = new System.Drawing.Point(437, 8);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(45, 17);
            this.lblEdad.TabIndex = 3;
            this.lblEdad.Text = "Edad:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.78016F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.21984F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.tableLayoutPanel2.Controls.Add(this.dtTmFechaNac, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblApPaterno, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblFechaNac, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtBxApPaterno, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 78);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(672, 34);
            this.tableLayoutPanel2.TabIndex = 37;
            // 
            // dtTmFechaNac
            // 
            this.dtTmFechaNac.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dtTmFechaNac.CalendarTitleBackColor = System.Drawing.Color.DarkSalmon;
            this.dtTmFechaNac.CalendarTrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(203)))), ((int)(((byte)(156)))));
            this.dtTmFechaNac.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtTmFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTmFechaNac.Location = new System.Drawing.Point(531, 5);
            this.dtTmFechaNac.Name = "dtTmFechaNac";
            this.dtTmFechaNac.Size = new System.Drawing.Size(138, 23);
            this.dtTmFechaNac.TabIndex = 8;
            // 
            // lblApPaterno
            // 
            this.lblApPaterno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblApPaterno.AutoSize = true;
            this.lblApPaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApPaterno.Location = new System.Drawing.Point(3, 8);
            this.lblApPaterno.Name = "lblApPaterno";
            this.lblApPaterno.Size = new System.Drawing.Size(119, 17);
            this.lblApPaterno.TabIndex = 5;
            this.lblApPaterno.Text = "Apellido Paterno:";
            // 
            // lblFechaNac
            // 
            this.lblFechaNac.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFechaNac.AutoSize = true;
            this.lblFechaNac.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaNac.Location = new System.Drawing.Point(380, 8);
            this.lblFechaNac.Name = "lblFechaNac";
            this.lblFechaNac.Size = new System.Drawing.Size(145, 17);
            this.lblFechaNac.TabIndex = 9;
            this.lblFechaNac.Text = "Fecha de Nacimiento:";
            // 
            // txtBxApPaterno
            // 
            this.txtBxApPaterno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxApPaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxApPaterno.Location = new System.Drawing.Point(128, 3);
            this.txtBxApPaterno.MaxLength = 20;
            this.txtBxApPaterno.Multiline = true;
            this.txtBxApPaterno.Name = "txtBxApPaterno";
            this.txtBxApPaterno.Size = new System.Drawing.Size(225, 28);
            this.txtBxApPaterno.TabIndex = 7;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 4;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 326F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel22.Controls.Add(this.txtBxCP, 3, 0);
            this.tableLayoutPanel22.Controls.Add(this.lblCP, 2, 0);
            this.tableLayoutPanel22.Controls.Add(this.txtBxEstado, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.lblEstado, 0, 0);
            this.tableLayoutPanel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel22.Location = new System.Drawing.Point(12, 362);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 1;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(672, 35);
            this.tableLayoutPanel22.TabIndex = 55;
            // 
            // txtBxCP
            // 
            this.txtBxCP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxCP.Location = new System.Drawing.Point(527, 3);
            this.txtBxCP.Multiline = true;
            this.txtBxCP.Name = "txtBxCP";
            this.txtBxCP.Size = new System.Drawing.Size(104, 29);
            this.txtBxCP.TabIndex = 51;
            // 
            // lblCP
            // 
            this.lblCP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCP.AutoSize = true;
            this.lblCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCP.Location = new System.Drawing.Point(470, 9);
            this.lblCP.Name = "lblCP";
            this.lblCP.Size = new System.Drawing.Size(51, 17);
            this.lblCP.TabIndex = 51;
            this.lblCP.Text = "C.P.:";
            // 
            // txtBxEstado
            // 
            this.txtBxEstado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxEstado.Location = new System.Drawing.Point(144, 3);
            this.txtBxEstado.Multiline = true;
            this.txtBxEstado.Name = "txtBxEstado";
            this.txtBxEstado.Size = new System.Drawing.Size(320, 29);
            this.txtBxEstado.TabIndex = 44;
            // 
            // lblEstado
            // 
            this.lblEstado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstado.AutoSize = true;
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.Location = new System.Drawing.Point(3, 9);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(135, 17);
            this.lblEstado.TabIndex = 43;
            this.lblEstado.Text = "Entidad Federativa:";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 504F));
            this.tableLayoutPanel21.Controls.Add(this.txtBxMunicipio, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.lblMunicipio, 0, 0);
            this.tableLayoutPanel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel21.Location = new System.Drawing.Point(12, 321);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(672, 35);
            this.tableLayoutPanel21.TabIndex = 54;
            // 
            // txtBxMunicipio
            // 
            this.txtBxMunicipio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxMunicipio.Location = new System.Drawing.Point(171, 3);
            this.txtBxMunicipio.Multiline = true;
            this.txtBxMunicipio.Name = "txtBxMunicipio";
            this.txtBxMunicipio.Size = new System.Drawing.Size(356, 29);
            this.txtBxMunicipio.TabIndex = 44;
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMunicipio.AutoSize = true;
            this.lblMunicipio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMunicipio.Location = new System.Drawing.Point(3, 9);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(162, 17);
            this.lblMunicipio.TabIndex = 43;
            this.lblMunicipio.Text = "Delegación o Municipio:";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 533F));
            this.tableLayoutPanel20.Controls.Add(this.txtBxColonia, 1, 0);
            this.tableLayoutPanel20.Controls.Add(this.lblColonia, 0, 0);
            this.tableLayoutPanel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel20.Location = new System.Drawing.Point(12, 280);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(672, 35);
            this.tableLayoutPanel20.TabIndex = 53;
            // 
            // txtBxColonia
            // 
            this.txtBxColonia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxColonia.Location = new System.Drawing.Point(142, 3);
            this.txtBxColonia.Multiline = true;
            this.txtBxColonia.Name = "txtBxColonia";
            this.txtBxColonia.Size = new System.Drawing.Size(356, 29);
            this.txtBxColonia.TabIndex = 44;
            // 
            // lblColonia
            // 
            this.lblColonia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColonia.AutoSize = true;
            this.lblColonia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColonia.Location = new System.Drawing.Point(3, 9);
            this.lblColonia.Name = "lblColonia";
            this.lblColonia.Size = new System.Drawing.Size(133, 17);
            this.lblColonia.TabIndex = 43;
            this.lblColonia.Text = "Colonia/Población:";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.lblDom, 0, 0);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(12, 198);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(672, 35);
            this.tableLayoutPanel19.TabIndex = 52;
            // 
            // lblDom
            // 
            this.lblDom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDom.AutoSize = true;
            this.lblDom.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDom.Location = new System.Drawing.Point(3, 8);
            this.lblDom.Name = "lblDom";
            this.lblDom.Size = new System.Drawing.Size(666, 18);
            this.lblDom.TabIndex = 43;
            this.lblDom.Text = "Domicilio Particular";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 4;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 448F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel18.Controls.Add(this.txtBxNum, 3, 0);
            this.tableLayoutPanel18.Controls.Add(this.lblNum, 2, 0);
            this.tableLayoutPanel18.Controls.Add(this.txtBxCalle, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.lblCalle, 0, 0);
            this.tableLayoutPanel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel18.Location = new System.Drawing.Point(12, 239);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(672, 35);
            this.tableLayoutPanel18.TabIndex = 51;
            // 
            // txtBxNum
            // 
            this.txtBxNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxNum.Location = new System.Drawing.Point(543, 3);
            this.txtBxNum.Multiline = true;
            this.txtBxNum.Name = "txtBxNum";
            this.txtBxNum.Size = new System.Drawing.Size(104, 29);
            this.txtBxNum.TabIndex = 47;
            // 
            // lblNum
            // 
            this.lblNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNum.AutoSize = true;
            this.lblNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNum.Location = new System.Drawing.Point(507, 9);
            this.lblNum.Name = "lblNum";
            this.lblNum.Size = new System.Drawing.Size(30, 17);
            this.lblNum.TabIndex = 47;
            this.lblNum.Text = "N°:";
            // 
            // txtBxCalle
            // 
            this.txtBxCalle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxCalle.Location = new System.Drawing.Point(59, 3);
            this.txtBxCalle.Multiline = true;
            this.txtBxCalle.Name = "txtBxCalle";
            this.txtBxCalle.Size = new System.Drawing.Size(356, 29);
            this.txtBxCalle.TabIndex = 44;
            // 
            // lblCalle
            // 
            this.lblCalle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCalle.AutoSize = true;
            this.lblCalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalle.Location = new System.Drawing.Point(3, 9);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(50, 17);
            this.lblCalle.TabIndex = 43;
            this.lblCalle.Text = "Calle:";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 326F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel5.Controls.Add(this.txtBxCPFam, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblCPFam, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtBxEstadoFam, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblEstadoFam, 0, 0);
            this.tableLayoutPanel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel5.Location = new System.Drawing.Point(12, 567);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel5.TabIndex = 60;
            // 
            // txtBxCPFam
            // 
            this.txtBxCPFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxCPFam.Location = new System.Drawing.Point(523, 3);
            this.txtBxCPFam.Multiline = true;
            this.txtBxCPFam.Name = "txtBxCPFam";
            this.txtBxCPFam.Size = new System.Drawing.Size(104, 29);
            this.txtBxCPFam.TabIndex = 51;
            // 
            // lblCPFam
            // 
            this.lblCPFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCPFam.AutoSize = true;
            this.lblCPFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPFam.Location = new System.Drawing.Point(466, 9);
            this.lblCPFam.Name = "lblCPFam";
            this.lblCPFam.Size = new System.Drawing.Size(51, 17);
            this.lblCPFam.TabIndex = 51;
            this.lblCPFam.Text = "C.P.:";
            // 
            // txtBxEstadoFam
            // 
            this.txtBxEstadoFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxEstadoFam.Location = new System.Drawing.Point(140, 3);
            this.txtBxEstadoFam.Multiline = true;
            this.txtBxEstadoFam.Name = "txtBxEstadoFam";
            this.txtBxEstadoFam.Size = new System.Drawing.Size(320, 29);
            this.txtBxEstadoFam.TabIndex = 44;
            // 
            // lblEstadoFam
            // 
            this.lblEstadoFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstadoFam.AutoSize = true;
            this.lblEstadoFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoFam.Location = new System.Drawing.Point(3, 9);
            this.lblEstadoFam.Name = "lblEstadoFam";
            this.lblEstadoFam.Size = new System.Drawing.Size(131, 17);
            this.lblEstadoFam.TabIndex = 43;
            this.lblEstadoFam.Text = "Entidad Federativa:";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 504F));
            this.tableLayoutPanel6.Controls.Add(this.txtBxMunicipioFam, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.lblMunicipioFam, 0, 0);
            this.tableLayoutPanel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel6.Location = new System.Drawing.Point(12, 526);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel6.TabIndex = 59;
            // 
            // txtBxMunicipioFam
            // 
            this.txtBxMunicipioFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxMunicipioFam.Location = new System.Drawing.Point(167, 3);
            this.txtBxMunicipioFam.Multiline = true;
            this.txtBxMunicipioFam.Name = "txtBxMunicipioFam";
            this.txtBxMunicipioFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxMunicipioFam.TabIndex = 44;
            // 
            // lblMunicipioFam
            // 
            this.lblMunicipioFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMunicipioFam.AutoSize = true;
            this.lblMunicipioFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMunicipioFam.Location = new System.Drawing.Point(3, 9);
            this.lblMunicipioFam.Name = "lblMunicipioFam";
            this.lblMunicipioFam.Size = new System.Drawing.Size(158, 17);
            this.lblMunicipioFam.TabIndex = 43;
            this.lblMunicipioFam.Text = "Delegación o Municipio:";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 533F));
            this.tableLayoutPanel7.Controls.Add(this.txtBxColoniaFam, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.lblColoniaFam, 0, 0);
            this.tableLayoutPanel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel7.Location = new System.Drawing.Point(12, 485);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel7.TabIndex = 58;
            // 
            // txtBxColoniaFam
            // 
            this.txtBxColoniaFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxColoniaFam.Location = new System.Drawing.Point(138, 3);
            this.txtBxColoniaFam.Multiline = true;
            this.txtBxColoniaFam.Name = "txtBxColoniaFam";
            this.txtBxColoniaFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxColoniaFam.TabIndex = 44;
            // 
            // lblColoniaFam
            // 
            this.lblColoniaFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColoniaFam.AutoSize = true;
            this.lblColoniaFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColoniaFam.Location = new System.Drawing.Point(3, 9);
            this.lblColoniaFam.Name = "lblColoniaFam";
            this.lblColoniaFam.Size = new System.Drawing.Size(129, 17);
            this.lblColoniaFam.TabIndex = 43;
            this.lblColoniaFam.Text = "Colonia/Población:";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.lblDomFam, 0, 0);
            this.tableLayoutPanel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel8.Location = new System.Drawing.Point(12, 403);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel8.TabIndex = 57;
            // 
            // lblDomFam
            // 
            this.lblDomFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDomFam.AutoSize = true;
            this.lblDomFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDomFam.Location = new System.Drawing.Point(3, 8);
            this.lblDomFam.Name = "lblDomFam";
            this.lblDomFam.Size = new System.Drawing.Size(662, 18);
            this.lblDomFam.TabIndex = 43;
            this.lblDomFam.Text = "Domicilio Familiar";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 448F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel9.Controls.Add(this.txtBxNumFam, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.lblNumFam, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtBxCalleFam, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.lblCalleFam, 0, 0);
            this.tableLayoutPanel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel9.Location = new System.Drawing.Point(12, 444);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel9.TabIndex = 56;
            // 
            // txtBxNumFam
            // 
            this.txtBxNumFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxNumFam.Location = new System.Drawing.Point(539, 3);
            this.txtBxNumFam.Multiline = true;
            this.txtBxNumFam.Name = "txtBxNumFam";
            this.txtBxNumFam.Size = new System.Drawing.Size(104, 29);
            this.txtBxNumFam.TabIndex = 47;
            // 
            // lblNumFam
            // 
            this.lblNumFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumFam.AutoSize = true;
            this.lblNumFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumFam.Location = new System.Drawing.Point(503, 9);
            this.lblNumFam.Name = "lblNumFam";
            this.lblNumFam.Size = new System.Drawing.Size(30, 17);
            this.lblNumFam.TabIndex = 47;
            this.lblNumFam.Text = "N°:";
            // 
            // txtBxCalleFam
            // 
            this.txtBxCalleFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxCalleFam.Location = new System.Drawing.Point(55, 3);
            this.txtBxCalleFam.Multiline = true;
            this.txtBxCalleFam.Name = "txtBxCalleFam";
            this.txtBxCalleFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxCalleFam.TabIndex = 44;
            // 
            // lblCalleFam
            // 
            this.lblCalleFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCalleFam.AutoSize = true;
            this.lblCalleFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalleFam.Location = new System.Drawing.Point(3, 9);
            this.lblCalleFam.Name = "lblCalleFam";
            this.lblCalleFam.Size = new System.Drawing.Size(46, 17);
            this.lblCalleFam.TabIndex = 43;
            this.lblCalleFam.Text = "Calle:";
            // 
            // bttnCancelar
            // 
            this.bttnCancelar.BackColor = System.Drawing.Color.Orange;
            this.bttnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnCancelar.ForeColor = System.Drawing.Color.Black;
            this.bttnCancelar.Location = new System.Drawing.Point(372, 763);
            this.bttnCancelar.Name = "bttnCancelar";
            this.bttnCancelar.Size = new System.Drawing.Size(100, 50);
            this.bttnCancelar.TabIndex = 62;
            this.bttnCancelar.Text = "Cancelar";
            this.bttnCancelar.UseVisualStyleBackColor = false;
            this.bttnCancelar.Click += new System.EventHandler(this.clickCancelar);
            this.bttnCancelar.MouseEnter += new System.EventHandler(this.mouseCancelarEnter);
            this.bttnCancelar.MouseLeave += new System.EventHandler(this.mouseCancelarLeave);
            // 
            // bttnGuardar
            // 
            this.bttnGuardar.BackColor = System.Drawing.Color.RoyalBlue;
            this.bttnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnGuardar.ForeColor = System.Drawing.Color.White;
            this.bttnGuardar.Location = new System.Drawing.Point(237, 763);
            this.bttnGuardar.Name = "bttnGuardar";
            this.bttnGuardar.Size = new System.Drawing.Size(100, 50);
            this.bttnGuardar.TabIndex = 61;
            this.bttnGuardar.Text = "Guardar";
            this.bttnGuardar.UseVisualStyleBackColor = false;
            this.bttnGuardar.Click += new System.EventHandler(this.bttnGuardar_Click);
            this.bttnGuardar.MouseEnter += new System.EventHandler(this.mouseGuardarEnter);
            this.bttnGuardar.MouseLeave += new System.EventHandler(this.mouseGuardarLeave);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.11309F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.8869F));
            this.tableLayoutPanel10.Controls.Add(this.lblTipoEgreso, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtBxTipoEgreso, 1, 0);
            this.tableLayoutPanel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel10.Location = new System.Drawing.Point(12, 608);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(672, 36);
            this.tableLayoutPanel10.TabIndex = 63;
            // 
            // lblTipoEgreso
            // 
            this.lblTipoEgreso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoEgreso.AutoSize = true;
            this.lblTipoEgreso.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoEgreso.Location = new System.Drawing.Point(3, 9);
            this.lblTipoEgreso.Name = "lblTipoEgreso";
            this.lblTipoEgreso.Size = new System.Drawing.Size(108, 17);
            this.lblTipoEgreso.TabIndex = 64;
            this.lblTipoEgreso.Text = "Tipo de egreso:";
            // 
            // txtBxTipoEgreso
            // 
            this.txtBxTipoEgreso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxTipoEgreso.Location = new System.Drawing.Point(117, 3);
            this.txtBxTipoEgreso.MaxLength = 25;
            this.txtBxTipoEgreso.Multiline = true;
            this.txtBxTipoEgreso.Name = "txtBxTipoEgreso";
            this.txtBxTipoEgreso.Size = new System.Drawing.Size(286, 30);
            this.txtBxTipoEgreso.TabIndex = 64;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.11309F));
            this.tableLayoutPanel11.Controls.Add(this.lblEstaGral, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtBxEstadoGral, 0, 1);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(12, 650);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(672, 95);
            this.tableLayoutPanel11.TabIndex = 64;
            // 
            // lblEstaGral
            // 
            this.lblEstaGral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstaGral.AutoSize = true;
            this.lblEstaGral.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstaGral.Location = new System.Drawing.Point(3, 9);
            this.lblEstaGral.Name = "lblEstaGral";
            this.lblEstaGral.Size = new System.Drawing.Size(666, 17);
            this.lblEstaGral.TabIndex = 64;
            this.lblEstaGral.Text = "Estado general:";
            // 
            // txtBxEstadoGral
            // 
            this.txtBxEstadoGral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxEstadoGral.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxEstadoGral.Location = new System.Drawing.Point(3, 38);
            this.txtBxEstadoGral.MaxLength = 400;
            this.txtBxEstadoGral.Multiline = true;
            this.txtBxEstadoGral.Name = "txtBxEstadoGral";
            this.txtBxEstadoGral.Size = new System.Drawing.Size(666, 54);
            this.txtBxEstadoGral.TabIndex = 64;
            // 
            // frmNuevoEgreso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(696, 531);
            this.Controls.Add(this.tableLayoutPanel11);
            this.Controls.Add(this.tableLayoutPanel10);
            this.Controls.Add(this.bttnCancelar);
            this.Controls.Add(this.bttnGuardar);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Controls.Add(this.tableLayoutPanel7);
            this.Controls.Add(this.tableLayoutPanel8);
            this.Controls.Add(this.tableLayoutPanel9);
            this.Controls.Add(this.tableLayoutPanel22);
            this.Controls.Add(this.tableLayoutPanel21);
            this.Controls.Add(this.tableLayoutPanel20);
            this.Controls.Add(this.tableLayoutPanel19);
            this.Controls.Add(this.tableLayoutPanel18);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "frmNuevoEgreso";
            this.ShowIcon = false;
            this.Text = "Nuevo Egreso";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmNuevoEgreso_FormClosed);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnEdad)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label lblOcupacion;
        private System.Windows.Forms.TextBox txtBxOcupacion;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblApMaterno;
        private System.Windows.Forms.TextBox txtBxApMaterno;
        private System.Windows.Forms.ComboBox cmbBxEstadoCivil;
        private System.Windows.Forms.ComboBox cmbBxEscolaridad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox cmbBxSexo;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.NumericUpDown numUpDnEdad;
        private System.Windows.Forms.TextBox txtBxNombre;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DateTimePicker dtTmFechaNac;
        private System.Windows.Forms.Label lblApPaterno;
        private System.Windows.Forms.Label lblFechaNac;
        private System.Windows.Forms.TextBox txtBxApPaterno;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.TextBox txtBxCP;
        private System.Windows.Forms.Label lblCP;
        private System.Windows.Forms.TextBox txtBxEstado;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.TextBox txtBxMunicipio;
        private System.Windows.Forms.Label lblMunicipio;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.TextBox txtBxColonia;
        private System.Windows.Forms.Label lblColonia;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Label lblDom;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.TextBox txtBxNum;
        private System.Windows.Forms.Label lblNum;
        private System.Windows.Forms.TextBox txtBxCalle;
        private System.Windows.Forms.Label lblCalle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox txtBxCPFam;
        private System.Windows.Forms.Label lblCPFam;
        private System.Windows.Forms.TextBox txtBxEstadoFam;
        private System.Windows.Forms.Label lblEstadoFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox txtBxMunicipioFam;
        private System.Windows.Forms.Label lblMunicipioFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TextBox txtBxColoniaFam;
        private System.Windows.Forms.Label lblColoniaFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label lblDomFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TextBox txtBxNumFam;
        private System.Windows.Forms.Label lblNumFam;
        private System.Windows.Forms.TextBox txtBxCalleFam;
        private System.Windows.Forms.Label lblCalleFam;
        private System.Windows.Forms.Button bttnCancelar;
        private System.Windows.Forms.Button bttnGuardar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label lblTipoEgreso;
        private System.Windows.Forms.TextBox txtBxTipoEgreso;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label lblEstaGral;
        private System.Windows.Forms.TextBox txtBxEstadoGral;
    }
}