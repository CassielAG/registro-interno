﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroInterno
{
    public partial class frmAgregarInterno : Form
    {
        conexion con = new conexion();

        public frmAgregarInterno()
        {
            InitializeComponent();
        }

        private void AgregarInterno_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void clickCancelar(object sender, EventArgs e)
        {
            this.Hide();
            frmInternos internos = new frmInternos("mostrarInternos", 3);
            internos.Show();
        }

        private void mouseCancelarEnter(object sender, EventArgs e)
        {
            bttnCancelar.BackColor = Color.NavajoWhite;
        }

        private void mouseCancelarLeave(object sender, EventArgs e)
        {
            bttnCancelar.BackColor = Color.Orange;
        }

        private void mouseGuardarEnter(object sender, EventArgs e)
        {
            bttnGuardar.BackColor = Color.CornflowerBlue;
        }

        private void mouseGuardarLeave(object sender, EventArgs e)
        {
            bttnGuardar.BackColor = Color.RoyalBlue;
        }

        private void bttnGuardar_Click(object sender, EventArgs e)
        {
            string[] camposInternos = {
                "vNOMBRE", "vAP_PATERNO", "vAP_MATERNO", "vEDAD", "vSEXO", "vFECHA_NAC"
                , "vESTADO_CIVIL", "vESCOLARIDAD", "vOCUPACION", "vSERV_MED", "vNSS"
            };

            string[] valoresInternos =
            {
                txtBxNombre.Text, txtBxApPaterno.Text, txtBxApMaterno.Text, numUpDnEdad.Value.ToString(), cmbBxSexo.Text,dtTmFechaNac.Text
                , cmbBxEstadoCivil.Text, cmbBxEscolaridad.Text, txtBxOcupacion.Text, txtBxServMedico.Text, txtBxNSS.Text
            };

            con.Comando("INSERTARINTERNOS", camposInternos, valoresInternos);

            if (!valoresInternos.Contains(""))
            {
                MessageBox.Show("Interno guardado");
                this.Hide();
                frmInternos internos = new frmInternos("mostrarInternos", 3);
                internos.Show();
            }
            else
            {
                MessageBox.Show("ALGO FALTÓ");
            }
        }
    }
}
