﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RegistroInterno.Properties;

namespace RegistroInterno
{
    public partial class frmCambiarContrasena : Form
    {
        public frmCambiarContrasena()
        {
            InitializeComponent();
        }

        private void mouseGuardarEnter(object sender, EventArgs e)
        {
            btnGuardar.BackColor = Color.CornflowerBlue;
        }

        private void mouseGuardarLeave(object sender, EventArgs e)
        {
            btnGuardar.BackColor = Color.RoyalBlue;
        }

        private void mouseCancelarEnter(object sender, EventArgs e)
        {
            btnCancelar.BackColor = Color.NavajoWhite;
        }

        private void mouseCancelarLeave(object sender, EventArgs e)
        {
            btnCancelar.BackColor = Color.Orange;
        }

        private void clickGuardar(object sender, EventArgs e)
        {
            if(txtBxPregunta.Text == "Jose")
            {
                Settings.Default.Contrasenia = txtBxContrasena.Text;
                Settings.Default.Save();

                MessageBox.Show("CONTRASEÑA GUARDADA CORRECTAMENTE");

                this.Hide();
                frmMenu menu = new frmMenu();
                menu.Show();
            }
            else
            {
                MessageBox.Show("NOMBRE INCORRECTO");
            }
        }

        private void clickCancelar(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void cerrarForm(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }
    }
}
