﻿namespace RegistroInterno
{
    partial class frmInicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInicio));
            this.pictLogo = new System.Windows.Forms.PictureBox();
            this.txtBxContrasena = new System.Windows.Forms.TextBox();
            this.lblIngresa = new System.Windows.Forms.Label();
            this.lblBienvenido = new System.Windows.Forms.Label();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.chckBxMostrar = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pictLogo
            // 
            this.pictLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictLogo.Image")));
            this.pictLogo.Location = new System.Drawing.Point(100, 12);
            this.pictLogo.Name = "pictLogo";
            this.pictLogo.Size = new System.Drawing.Size(223, 177);
            this.pictLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictLogo.TabIndex = 0;
            this.pictLogo.TabStop = false;
            // 
            // txtBxContrasena
            // 
            this.txtBxContrasena.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBxContrasena.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxContrasena.Location = new System.Drawing.Point(100, 292);
            this.txtBxContrasena.MaximumSize = new System.Drawing.Size(350, 350);
            this.txtBxContrasena.Name = "txtBxContrasena";
            this.txtBxContrasena.Size = new System.Drawing.Size(223, 31);
            this.txtBxContrasena.TabIndex = 1;
            this.txtBxContrasena.TabStop = false;
            this.txtBxContrasena.UseSystemPasswordChar = true;
            this.txtBxContrasena.WordWrap = false;
            // 
            // lblIngresa
            // 
            this.lblIngresa.AutoSize = true;
            this.lblIngresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIngresa.Location = new System.Drawing.Point(120, 256);
            this.lblIngresa.Name = "lblIngresa";
            this.lblIngresa.Size = new System.Drawing.Size(187, 22);
            this.lblIngresa.TabIndex = 2;
            this.lblIngresa.Text = "Ingrese la contraseña:";
            // 
            // lblBienvenido
            // 
            this.lblBienvenido.AutoSize = true;
            this.lblBienvenido.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBienvenido.Location = new System.Drawing.Point(143, 204);
            this.lblBienvenido.Name = "lblBienvenido";
            this.lblBienvenido.Size = new System.Drawing.Size(144, 29);
            this.lblBienvenido.TabIndex = 3;
            this.lblBienvenido.Text = "Bienvenido";
            // 
            // btnIngresar
            // 
            this.btnIngresar.BackColor = System.Drawing.Color.Orange;
            this.btnIngresar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresar.ForeColor = System.Drawing.Color.Black;
            this.btnIngresar.Location = new System.Drawing.Point(148, 364);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(119, 39);
            this.btnIngresar.TabIndex = 4;
            this.btnIngresar.Text = "Ingresar";
            this.btnIngresar.UseVisualStyleBackColor = false;
            this.btnIngresar.Click += new System.EventHandler(this.ingresarClick);
            this.btnIngresar.MouseEnter += new System.EventHandler(this.mousEnter);
            this.btnIngresar.MouseLeave += new System.EventHandler(this.mouseLeave);
            // 
            // chckBxMostrar
            // 
            this.chckBxMostrar.AutoSize = true;
            this.chckBxMostrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckBxMostrar.Location = new System.Drawing.Point(148, 329);
            this.chckBxMostrar.Name = "chckBxMostrar";
            this.chckBxMostrar.Size = new System.Drawing.Size(132, 19);
            this.chckBxMostrar.TabIndex = 5;
            this.chckBxMostrar.Text = "Mostrar contraseña";
            this.chckBxMostrar.UseVisualStyleBackColor = true;
            this.chckBxMostrar.CheckedChanged += new System.EventHandler(this.chckBxMostrar_CheckedChanged);
            // 
            // frmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(434, 461);
            this.Controls.Add(this.chckBxMostrar);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.lblBienvenido);
            this.Controls.Add(this.lblIngresa);
            this.Controls.Add(this.txtBxContrasena);
            this.Controls.Add(this.pictLogo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(50, 50);
            this.MaximizeBox = false;
            this.Name = "frmInicio";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registro";
            ((System.ComponentModel.ISupportInitialize)(this.pictLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictLogo;
        private System.Windows.Forms.TextBox txtBxContrasena;
        private System.Windows.Forms.Label lblIngresa;
        private System.Windows.Forms.Label lblBienvenido;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.CheckBox chckBxMostrar;
    }
}

