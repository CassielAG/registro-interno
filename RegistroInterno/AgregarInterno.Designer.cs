﻿namespace RegistroInterno
{
    partial class frmAgregarInterno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBxNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.numUpDnEdad = new System.Windows.Forms.NumericUpDown();
            this.lblApPaterno = new System.Windows.Forms.Label();
            this.cmbBxSexo = new System.Windows.Forms.ComboBox();
            this.txtBxApPaterno = new System.Windows.Forms.TextBox();
            this.dtTmFechaNac = new System.Windows.Forms.DateTimePicker();
            this.lblFechaNac = new System.Windows.Forms.Label();
            this.lblApMaterno = new System.Windows.Forms.Label();
            this.txtBxApMaterno = new System.Windows.Forms.TextBox();
            this.cmbBxEstadoCivil = new System.Windows.Forms.ComboBox();
            this.cmbBxEscolaridad = new System.Windows.Forms.ComboBox();
            this.lblOcupacion = new System.Windows.Forms.Label();
            this.txtBxOcupacion = new System.Windows.Forms.TextBox();
            this.lblServMedico = new System.Windows.Forms.Label();
            this.txtBxServMedico = new System.Windows.Forms.TextBox();
            this.lblNSS = new System.Windows.Forms.Label();
            this.txtBxNSS = new System.Windows.Forms.TextBox();
            this.cmbBxIngreso = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.txtBxProviene = new System.Windows.Forms.TextBox();
            this.cmbBxAcude = new System.Windows.Forms.ComboBox();
            this.txtBxParentesco = new System.Windows.Forms.TextBox();
            this.lblParentesco = new System.Windows.Forms.Label();
            this.lblProviene = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxRelacion = new System.Windows.Forms.TextBox();
            this.lblRelacion = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.lblRevision = new System.Windows.Forms.Label();
            this.txtBxRevision = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.chckListMedicamento = new System.Windows.Forms.CheckedListBox();
            this.lblMedicamento = new System.Windows.Forms.Label();
            this.txtBxMedicamento = new System.Windows.Forms.TextBox();
            this.chckListEnfermedad = new System.Windows.Forms.CheckedListBox();
            this.lblEnfermedad = new System.Windows.Forms.Label();
            this.txtBxEnfermedades = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxDrogasIng = new System.Windows.Forms.TextBox();
            this.lblDrogasIng = new System.Windows.Forms.Label();
            this.chckListDrogasCons = new System.Windows.Forms.CheckedListBox();
            this.lblDrogasCons = new System.Windows.Forms.Label();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDatosFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxNombreFam = new System.Windows.Forms.TextBox();
            this.lblNombreFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxApPatFam = new System.Windows.Forms.TextBox();
            this.lblApPatFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxApMatFam = new System.Windows.Forms.TextBox();
            this.lblApMatFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxNumFam = new System.Windows.Forms.TextBox();
            this.lblNumFam = new System.Windows.Forms.Label();
            this.txtBxCalleFam = new System.Windows.Forms.TextBox();
            this.lblCalleFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDomFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxColoniaFam = new System.Windows.Forms.TextBox();
            this.lblColoniaFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxMunicipioFam = new System.Windows.Forms.TextBox();
            this.lblMunicipioFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxCPFam = new System.Windows.Forms.TextBox();
            this.lblCPFam = new System.Windows.Forms.Label();
            this.txtBxEstadoFam = new System.Windows.Forms.TextBox();
            this.lblEstadoFam = new System.Windows.Forms.Label();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.lblObser = new System.Windows.Forms.Label();
            this.txtBxObser = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.numUpDnAportación = new System.Windows.Forms.NumericUpDown();
            this.lblPromesa = new System.Windows.Forms.Label();
            this.bttnGuardar = new System.Windows.Forms.Button();
            this.bttnCancelar = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxCP = new System.Windows.Forms.TextBox();
            this.lblCP = new System.Windows.Forms.Label();
            this.txtBxEntFederativa = new System.Windows.Forms.TextBox();
            this.lblEntFederativa = new System.Windows.Forms.Label();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxMunicipio = new System.Windows.Forms.TextBox();
            this.lblMunicipio = new System.Windows.Forms.Label();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxColonia = new System.Windows.Forms.TextBox();
            this.lblColonia = new System.Windows.Forms.Label();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDomPart = new System.Windows.Forms.Label();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBxNum = new System.Windows.Forms.TextBox();
            this.lblNum = new System.Windows.Forms.Label();
            this.txtBxCalle = new System.Windows.Forms.TextBox();
            this.lblCalle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnEdad)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnAportación)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nuevo Interno";
            // 
            // txtBxNombre
            // 
            this.txtBxNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxNombre.Location = new System.Drawing.Point(105, 3);
            this.txtBxNombre.MaxLength = 20;
            this.txtBxNombre.Multiline = true;
            this.txtBxNombre.Name = "txtBxNombre";
            this.txtBxNombre.Size = new System.Drawing.Size(294, 28);
            this.txtBxNombre.TabIndex = 1;
            // 
            // lblNombre
            // 
            this.lblNombre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(3, 8);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(96, 17);
            this.lblNombre.TabIndex = 2;
            this.lblNombre.Text = "Nombre (s):";
            // 
            // lblEdad
            // 
            this.lblEdad.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEdad.AutoSize = true;
            this.lblEdad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdad.Location = new System.Drawing.Point(435, 8);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(45, 17);
            this.lblEdad.TabIndex = 3;
            this.lblEdad.Text = "Edad:";
            // 
            // numUpDnEdad
            // 
            this.numUpDnEdad.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.numUpDnEdad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUpDnEdad.Location = new System.Drawing.Point(489, 5);
            this.numUpDnEdad.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numUpDnEdad.Name = "numUpDnEdad";
            this.numUpDnEdad.Size = new System.Drawing.Size(55, 23);
            this.numUpDnEdad.TabIndex = 4;
            this.numUpDnEdad.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // lblApPaterno
            // 
            this.lblApPaterno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblApPaterno.AutoSize = true;
            this.lblApPaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApPaterno.Location = new System.Drawing.Point(3, 8);
            this.lblApPaterno.Name = "lblApPaterno";
            this.lblApPaterno.Size = new System.Drawing.Size(118, 17);
            this.lblApPaterno.TabIndex = 5;
            this.lblApPaterno.Text = "Apellido Paterno:";
            // 
            // cmbBxSexo
            // 
            this.cmbBxSexo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbBxSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBxSexo.FormattingEnabled = true;
            this.cmbBxSexo.Items.AddRange(new object[] {
            "Sexo",
            "Masculino",
            "Femenino"});
            this.cmbBxSexo.Location = new System.Drawing.Point(572, 4);
            this.cmbBxSexo.MaxDropDownItems = 3;
            this.cmbBxSexo.Name = "cmbBxSexo";
            this.cmbBxSexo.Size = new System.Drawing.Size(93, 24);
            this.cmbBxSexo.TabIndex = 6;
            this.cmbBxSexo.Text = "Sexo";
            // 
            // txtBxApPaterno
            // 
            this.txtBxApPaterno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxApPaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxApPaterno.Location = new System.Drawing.Point(127, 3);
            this.txtBxApPaterno.MaxLength = 20;
            this.txtBxApPaterno.Multiline = true;
            this.txtBxApPaterno.Name = "txtBxApPaterno";
            this.txtBxApPaterno.Size = new System.Drawing.Size(216, 28);
            this.txtBxApPaterno.TabIndex = 7;
            // 
            // dtTmFechaNac
            // 
            this.dtTmFechaNac.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dtTmFechaNac.CalendarTitleBackColor = System.Drawing.Color.DarkSalmon;
            this.dtTmFechaNac.CalendarTrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(203)))), ((int)(((byte)(156)))));
            this.dtTmFechaNac.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtTmFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTmFechaNac.Location = new System.Drawing.Point(529, 5);
            this.dtTmFechaNac.Name = "dtTmFechaNac";
            this.dtTmFechaNac.Size = new System.Drawing.Size(136, 23);
            this.dtTmFechaNac.TabIndex = 8;
            // 
            // lblFechaNac
            // 
            this.lblFechaNac.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFechaNac.AutoSize = true;
            this.lblFechaNac.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaNac.Location = new System.Drawing.Point(378, 8);
            this.lblFechaNac.Name = "lblFechaNac";
            this.lblFechaNac.Size = new System.Drawing.Size(145, 17);
            this.lblFechaNac.TabIndex = 9;
            this.lblFechaNac.Text = "Fecha de Nacimiento:";
            // 
            // lblApMaterno
            // 
            this.lblApMaterno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblApMaterno.AutoSize = true;
            this.lblApMaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApMaterno.Location = new System.Drawing.Point(3, 8);
            this.lblApMaterno.Name = "lblApMaterno";
            this.lblApMaterno.Size = new System.Drawing.Size(119, 17);
            this.lblApMaterno.TabIndex = 10;
            this.lblApMaterno.Text = "Apellido Materno:";
            // 
            // txtBxApMaterno
            // 
            this.txtBxApMaterno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxApMaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxApMaterno.Location = new System.Drawing.Point(128, 3);
            this.txtBxApMaterno.MaxLength = 20;
            this.txtBxApMaterno.Multiline = true;
            this.txtBxApMaterno.Name = "txtBxApMaterno";
            this.txtBxApMaterno.Size = new System.Drawing.Size(215, 28);
            this.txtBxApMaterno.TabIndex = 11;
            // 
            // cmbBxEstadoCivil
            // 
            this.cmbBxEstadoCivil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBxEstadoCivil.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBxEstadoCivil.FormattingEnabled = true;
            this.cmbBxEstadoCivil.Items.AddRange(new object[] {
            "Estado civil",
            "Casado",
            "Soltero"});
            this.cmbBxEstadoCivil.Location = new System.Drawing.Point(375, 4);
            this.cmbBxEstadoCivil.MaxDropDownItems = 3;
            this.cmbBxEstadoCivil.Name = "cmbBxEstadoCivil";
            this.cmbBxEstadoCivil.Size = new System.Drawing.Size(140, 24);
            this.cmbBxEstadoCivil.TabIndex = 12;
            this.cmbBxEstadoCivil.Text = "Estado civil";
            // 
            // cmbBxEscolaridad
            // 
            this.cmbBxEscolaridad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBxEscolaridad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBxEscolaridad.FormattingEnabled = true;
            this.cmbBxEscolaridad.Items.AddRange(new object[] {
            "Escolaridad",
            "Primaria",
            "Primaria trunca",
            "Secundaria",
            "Secundaria trunca",
            "Preparatoria",
            "Preparatoria trunca",
            "Licenciatura",
            "Maestria",
            "Doctorado"});
            this.cmbBxEscolaridad.Location = new System.Drawing.Point(521, 4);
            this.cmbBxEscolaridad.MaxDropDownItems = 3;
            this.cmbBxEscolaridad.Name = "cmbBxEscolaridad";
            this.cmbBxEscolaridad.Size = new System.Drawing.Size(150, 24);
            this.cmbBxEscolaridad.TabIndex = 13;
            this.cmbBxEscolaridad.Text = "Escolaridad";
            // 
            // lblOcupacion
            // 
            this.lblOcupacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOcupacion.AutoSize = true;
            this.lblOcupacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOcupacion.Location = new System.Drawing.Point(3, 8);
            this.lblOcupacion.Name = "lblOcupacion";
            this.lblOcupacion.Size = new System.Drawing.Size(90, 17);
            this.lblOcupacion.TabIndex = 14;
            this.lblOcupacion.Text = "Ocupación:";
            // 
            // txtBxOcupacion
            // 
            this.txtBxOcupacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxOcupacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxOcupacion.Location = new System.Drawing.Point(99, 3);
            this.txtBxOcupacion.MaxLength = 50;
            this.txtBxOcupacion.Multiline = true;
            this.txtBxOcupacion.Name = "txtBxOcupacion";
            this.txtBxOcupacion.Size = new System.Drawing.Size(529, 28);
            this.txtBxOcupacion.TabIndex = 15;
            // 
            // lblServMedico
            // 
            this.lblServMedico.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblServMedico.AutoSize = true;
            this.lblServMedico.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServMedico.Location = new System.Drawing.Point(3, 7);
            this.lblServMedico.Name = "lblServMedico";
            this.lblServMedico.Size = new System.Drawing.Size(111, 17);
            this.lblServMedico.TabIndex = 16;
            this.lblServMedico.Text = "Servicio Médico:";
            // 
            // txtBxServMedico
            // 
            this.txtBxServMedico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxServMedico.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxServMedico.Location = new System.Drawing.Point(120, 3);
            this.txtBxServMedico.MaxLength = 6;
            this.txtBxServMedico.Multiline = true;
            this.txtBxServMedico.Name = "txtBxServMedico";
            this.txtBxServMedico.Size = new System.Drawing.Size(186, 25);
            this.txtBxServMedico.TabIndex = 17;
            // 
            // lblNSS
            // 
            this.lblNSS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNSS.AutoSize = true;
            this.lblNSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNSS.Location = new System.Drawing.Point(334, 7);
            this.lblNSS.Name = "lblNSS";
            this.lblNSS.Size = new System.Drawing.Size(111, 17);
            this.lblNSS.TabIndex = 18;
            this.lblNSS.Text = "N° de Afiliación:";
            // 
            // txtBxNSS
            // 
            this.txtBxNSS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxNSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxNSS.Location = new System.Drawing.Point(451, 3);
            this.txtBxNSS.MaxLength = 12;
            this.txtBxNSS.Multiline = true;
            this.txtBxNSS.Name = "txtBxNSS";
            this.txtBxNSS.Size = new System.Drawing.Size(203, 25);
            this.txtBxNSS.TabIndex = 19;
            // 
            // cmbBxIngreso
            // 
            this.cmbBxIngreso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBxIngreso.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBxIngreso.FormattingEnabled = true;
            this.cmbBxIngreso.Items.AddRange(new object[] {
            "Tipo de ingreso",
            "Voluntario",
            "Involuntario",
            "Obligatorio"});
            this.cmbBxIngreso.Location = new System.Drawing.Point(3, 5);
            this.cmbBxIngreso.MaxDropDownItems = 3;
            this.cmbBxIngreso.Name = "cmbBxIngreso";
            this.cmbBxIngreso.Size = new System.Drawing.Size(134, 24);
            this.cmbBxIngreso.TabIndex = 22;
            this.cmbBxIngreso.Text = "Tipo de ingreso";
            // 
            // comboBox5
            // 
            this.comboBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "Proviene",
            "Domicilio Particular",
            "Institución Pública",
            "Institución Privada",
            "Hospital de Salud Mental ",
            "Centro de Readaptación Social",
            "Otro"});
            this.comboBox5.Location = new System.Drawing.Point(143, 5);
            this.comboBox5.MaxDropDownItems = 3;
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(161, 24);
            this.comboBox5.TabIndex = 23;
            this.comboBox5.Text = "Proviene";
            // 
            // txtBxProviene
            // 
            this.txtBxProviene.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxProviene.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxProviene.Location = new System.Drawing.Point(401, 3);
            this.txtBxProviene.MaxLength = 50;
            this.txtBxProviene.Multiline = true;
            this.txtBxProviene.Name = "txtBxProviene";
            this.txtBxProviene.Size = new System.Drawing.Size(239, 28);
            this.txtBxProviene.TabIndex = 24;
            // 
            // cmbBxAcude
            // 
            this.cmbBxAcude.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbBxAcude.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBxAcude.FormattingEnabled = true;
            this.cmbBxAcude.Items.AddRange(new object[] {
            "Acude",
            "Solo",
            "Amigo",
            "Vecino",
            "Familiar"});
            this.cmbBxAcude.Location = new System.Drawing.Point(3, 5);
            this.cmbBxAcude.MaxDropDownItems = 3;
            this.cmbBxAcude.Name = "cmbBxAcude";
            this.cmbBxAcude.Size = new System.Drawing.Size(148, 24);
            this.cmbBxAcude.TabIndex = 25;
            this.cmbBxAcude.Text = "Acude";
            // 
            // txtBxParentesco
            // 
            this.txtBxParentesco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxParentesco.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxParentesco.Location = new System.Drawing.Point(247, 3);
            this.txtBxParentesco.MaxLength = 20;
            this.txtBxParentesco.Multiline = true;
            this.txtBxParentesco.Name = "txtBxParentesco";
            this.txtBxParentesco.Size = new System.Drawing.Size(260, 29);
            this.txtBxParentesco.TabIndex = 26;
            // 
            // lblParentesco
            // 
            this.lblParentesco.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParentesco.AutoSize = true;
            this.lblParentesco.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParentesco.Location = new System.Drawing.Point(157, 9);
            this.lblParentesco.Name = "lblParentesco";
            this.lblParentesco.Size = new System.Drawing.Size(84, 17);
            this.lblParentesco.TabIndex = 27;
            this.lblParentesco.Text = "Parentesco:";
            // 
            // lblProviene
            // 
            this.lblProviene.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProviene.AutoSize = true;
            this.lblProviene.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProviene.Location = new System.Drawing.Point(310, 8);
            this.lblProviene.Name = "lblProviene";
            this.lblProviene.Size = new System.Drawing.Size(85, 17);
            this.lblProviene.TabIndex = 28;
            this.lblProviene.Text = "Especifique:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.83178F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.16822F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.Controls.Add(this.cmbBxSexo, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNombre, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.numUpDnEdad, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtBxNombre, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblEdad, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 38);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(668, 34);
            this.tableLayoutPanel1.TabIndex = 29;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.78016F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.21984F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel2.Controls.Add(this.dtTmFechaNac, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblApPaterno, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblFechaNac, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtBxApPaterno, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 78);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 34);
            this.tableLayoutPanel2.TabIndex = 30;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.68984F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.31016F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 146F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 154F));
            this.tableLayoutPanel3.Controls.Add(this.lblApMaterno, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtBxApMaterno, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmbBxEstadoCivil, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmbBxEscolaridad, 3, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(12, 118);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(674, 34);
            this.tableLayoutPanel3.TabIndex = 31;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 572F));
            this.tableLayoutPanel4.Controls.Add(this.lblOcupacion, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtBxOcupacion, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(12, 158);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(668, 34);
            this.tableLayoutPanel4.TabIndex = 32;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.31157F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.68843F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 219F));
            this.tableLayoutPanel5.Controls.Add(this.lblServMedico, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtBxServMedico, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblNSS, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtBxNSS, 3, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(12, 198);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(668, 31);
            this.tableLayoutPanel5.TabIndex = 33;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.48387F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.51613F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 272F));
            this.tableLayoutPanel7.Controls.Add(this.cmbBxIngreso, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.comboBox5, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.lblProviene, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtBxProviene, 3, 0);
            this.tableLayoutPanel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel7.Location = new System.Drawing.Point(12, 440);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(671, 34);
            this.tableLayoutPanel7.TabIndex = 35;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.15789F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.84211F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 426F));
            this.tableLayoutPanel8.Controls.Add(this.cmbBxAcude, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.lblParentesco, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtBxParentesco, 2, 0);
            this.tableLayoutPanel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel8.Location = new System.Drawing.Point(12, 480);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(671, 35);
            this.tableLayoutPanel8.TabIndex = 36;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.97006F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.02994F));
            this.tableLayoutPanel9.Controls.Add(this.txtBxRelacion, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.lblRelacion, 0, 0);
            this.tableLayoutPanel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel9.Location = new System.Drawing.Point(12, 521);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(671, 34);
            this.tableLayoutPanel9.TabIndex = 37;
            // 
            // txtBxRelacion
            // 
            this.txtBxRelacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxRelacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxRelacion.Location = new System.Drawing.Point(103, 3);
            this.txtBxRelacion.MaxLength = 20;
            this.txtBxRelacion.Multiline = true;
            this.txtBxRelacion.Name = "txtBxRelacion";
            this.txtBxRelacion.Size = new System.Drawing.Size(236, 28);
            this.txtBxRelacion.TabIndex = 40;
            // 
            // lblRelacion
            // 
            this.lblRelacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRelacion.AutoSize = true;
            this.lblRelacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelacion.Location = new System.Drawing.Point(3, 8);
            this.lblRelacion.Name = "lblRelacion";
            this.lblRelacion.Size = new System.Drawing.Size(94, 17);
            this.lblRelacion.TabIndex = 40;
            this.lblRelacion.Text = "Otra relación:";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Location = new System.Drawing.Point(36, 577);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(76, 0);
            this.tableLayoutPanel10.TabIndex = 38;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.3454F));
            this.tableLayoutPanel11.Controls.Add(this.lblRevision, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtBxRevision, 0, 1);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(12, 561);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(658, 116);
            this.tableLayoutPanel11.TabIndex = 39;
            // 
            // lblRevision
            // 
            this.lblRevision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRevision.AutoSize = true;
            this.lblRevision.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRevision.Location = new System.Drawing.Point(3, 6);
            this.lblRevision.Name = "lblRevision";
            this.lblRevision.Size = new System.Drawing.Size(652, 17);
            this.lblRevision.TabIndex = 40;
            this.lblRevision.Text = "Revisión Física:";
            // 
            // txtBxRevision
            // 
            this.txtBxRevision.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxRevision.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxRevision.Location = new System.Drawing.Point(3, 33);
            this.txtBxRevision.MaxLength = 400;
            this.txtBxRevision.Multiline = true;
            this.txtBxRevision.Name = "txtBxRevision";
            this.txtBxRevision.Size = new System.Drawing.Size(652, 80);
            this.txtBxRevision.TabIndex = 41;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.26093F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 333F));
            this.tableLayoutPanel12.Controls.Add(this.chckListMedicamento, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.lblMedicamento, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtBxMedicamento, 1, 2);
            this.tableLayoutPanel12.Controls.Add(this.chckListEnfermedad, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.lblEnfermedad, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtBxEnfermedades, 0, 2);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(12, 683);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 3;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(658, 184);
            this.tableLayoutPanel12.TabIndex = 40;
            // 
            // chckListMedicamento
            // 
            this.chckListMedicamento.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chckListMedicamento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chckListMedicamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckListMedicamento.FormattingEnabled = true;
            this.chckListMedicamento.Items.AddRange(new object[] {
            "No",
            "Si, especifique:"});
            this.chckListMedicamento.Location = new System.Drawing.Point(328, 62);
            this.chckListMedicamento.Name = "chckListMedicamento";
            this.chckListMedicamento.Size = new System.Drawing.Size(126, 54);
            this.chckListMedicamento.TabIndex = 41;
            // 
            // lblMedicamento
            // 
            this.lblMedicamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMedicamento.AutoSize = true;
            this.lblMedicamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedicamento.Location = new System.Drawing.Point(328, 7);
            this.lblMedicamento.Name = "lblMedicamento";
            this.lblMedicamento.Size = new System.Drawing.Size(327, 16);
            this.lblMedicamento.TabIndex = 41;
            this.lblMedicamento.Text = "¿Toma algún medicamento?";
            // 
            // txtBxMedicamento
            // 
            this.txtBxMedicamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxMedicamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxMedicamento.Location = new System.Drawing.Point(328, 150);
            this.txtBxMedicamento.MaxLength = 250;
            this.txtBxMedicamento.Multiline = true;
            this.txtBxMedicamento.Name = "txtBxMedicamento";
            this.txtBxMedicamento.Size = new System.Drawing.Size(327, 30);
            this.txtBxMedicamento.TabIndex = 41;
            // 
            // chckListEnfermedad
            // 
            this.chckListEnfermedad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chckListEnfermedad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chckListEnfermedad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckListEnfermedad.FormattingEnabled = true;
            this.chckListEnfermedad.Items.AddRange(new object[] {
            "Embarazo",
            "Complicaciones Físicas",
            "Padecimientos Graves",
            "Complicaciones Psiquiátricas",
            "Enfermedad Contagiosa",
            "Otras, especifique:"});
            this.chckListEnfermedad.Location = new System.Drawing.Point(3, 34);
            this.chckListEnfermedad.Name = "chckListEnfermedad";
            this.chckListEnfermedad.Size = new System.Drawing.Size(319, 108);
            this.chckListEnfermedad.TabIndex = 41;
            // 
            // lblEnfermedad
            // 
            this.lblEnfermedad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnfermedad.AutoSize = true;
            this.lblEnfermedad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnfermedad.Location = new System.Drawing.Point(3, 7);
            this.lblEnfermedad.Name = "lblEnfermedad";
            this.lblEnfermedad.Size = new System.Drawing.Size(319, 16);
            this.lblEnfermedad.TabIndex = 42;
            this.lblEnfermedad.Text = "Presenta o ha presentado:";
            // 
            // txtBxEnfermedades
            // 
            this.txtBxEnfermedades.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxEnfermedades.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxEnfermedades.Location = new System.Drawing.Point(3, 150);
            this.txtBxEnfermedades.MaxLength = 250;
            this.txtBxEnfermedades.Multiline = true;
            this.txtBxEnfermedades.Name = "txtBxEnfermedades";
            this.txtBxEnfermedades.Size = new System.Drawing.Size(319, 30);
            this.txtBxEnfermedades.TabIndex = 41;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 331F));
            this.tableLayoutPanel13.Controls.Add(this.txtBxDrogasIng, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.lblDrogasIng, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.chckListDrogasCons, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.lblDrogasCons, 0, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(12, 873);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 119F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(658, 149);
            this.tableLayoutPanel13.TabIndex = 41;
            // 
            // txtBxDrogasIng
            // 
            this.txtBxDrogasIng.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxDrogasIng.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxDrogasIng.Location = new System.Drawing.Point(330, 33);
            this.txtBxDrogasIng.MaxLength = 300;
            this.txtBxDrogasIng.Multiline = true;
            this.txtBxDrogasIng.Name = "txtBxDrogasIng";
            this.txtBxDrogasIng.Size = new System.Drawing.Size(325, 113);
            this.txtBxDrogasIng.TabIndex = 43;
            // 
            // lblDrogasIng
            // 
            this.lblDrogasIng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDrogasIng.AutoSize = true;
            this.lblDrogasIng.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDrogasIng.Location = new System.Drawing.Point(330, 7);
            this.lblDrogasIng.Name = "lblDrogasIng";
            this.lblDrogasIng.Size = new System.Drawing.Size(325, 16);
            this.lblDrogasIng.TabIndex = 43;
            this.lblDrogasIng.Text = "Drogas por la que ingresa:";
            // 
            // chckListDrogasCons
            // 
            this.chckListDrogasCons.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckListDrogasCons.FormattingEnabled = true;
            this.chckListDrogasCons.Items.AddRange(new object[] {
            "Alcohol",
            "Marihuana",
            "Disolventes o inhalantes",
            "Alucinógenos",
            "Opio o Morfina",
            "Anfetaminas",
            "Rohypnol",
            "Bazuco o pasta base",
            "Tranquilizantes",
            "Cristal (Metanfetaminas)",
            "Sedantes",
            "Opiáceos (Analgésicos narcoticos)",
            "Cocaína"});
            this.chckListDrogasCons.Location = new System.Drawing.Point(3, 33);
            this.chckListDrogasCons.Name = "chckListDrogasCons";
            this.chckListDrogasCons.Size = new System.Drawing.Size(295, 94);
            this.chckListDrogasCons.TabIndex = 42;
            // 
            // lblDrogasCons
            // 
            this.lblDrogasCons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDrogasCons.AutoSize = true;
            this.lblDrogasCons.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDrogasCons.Location = new System.Drawing.Point(3, 7);
            this.lblDrogasCons.Name = "lblDrogasCons";
            this.lblDrogasCons.Size = new System.Drawing.Size(321, 16);
            this.lblDrogasCons.TabIndex = 42;
            this.lblDrogasCons.Text = "Drogas que consume:";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.lblDatosFam, 0, 0);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(12, 1028);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel14.TabIndex = 42;
            // 
            // lblDatosFam
            // 
            this.lblDatosFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDatosFam.AutoSize = true;
            this.lblDatosFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatosFam.Location = new System.Drawing.Point(3, 9);
            this.lblDatosFam.Name = "lblDatosFam";
            this.lblDatosFam.Size = new System.Drawing.Size(662, 16);
            this.lblDatosFam.TabIndex = 43;
            this.lblDatosFam.Text = "Datos del familiar o representante legal:";
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 579F));
            this.tableLayoutPanel15.Controls.Add(this.txtBxNombreFam, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.lblNombreFam, 0, 0);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(12, 1069);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel15.TabIndex = 43;
            // 
            // txtBxNombreFam
            // 
            this.txtBxNombreFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxNombreFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxNombreFam.Location = new System.Drawing.Point(92, 3);
            this.txtBxNombreFam.MaxLength = 50;
            this.txtBxNombreFam.Multiline = true;
            this.txtBxNombreFam.Name = "txtBxNombreFam";
            this.txtBxNombreFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxNombreFam.TabIndex = 44;
            // 
            // lblNombreFam
            // 
            this.lblNombreFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNombreFam.AutoSize = true;
            this.lblNombreFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreFam.Location = new System.Drawing.Point(3, 9);
            this.lblNombreFam.Name = "lblNombreFam";
            this.lblNombreFam.Size = new System.Drawing.Size(83, 17);
            this.lblNombreFam.TabIndex = 43;
            this.lblNombreFam.Text = "Nombre(s):";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 2;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 545F));
            this.tableLayoutPanel16.Controls.Add(this.txtBxApPatFam, 1, 0);
            this.tableLayoutPanel16.Controls.Add(this.lblApPatFam, 0, 0);
            this.tableLayoutPanel16.Location = new System.Drawing.Point(12, 1110);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel16.TabIndex = 44;
            // 
            // txtBxApPatFam
            // 
            this.txtBxApPatFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxApPatFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxApPatFam.Location = new System.Drawing.Point(126, 3);
            this.txtBxApPatFam.MaxLength = 30;
            this.txtBxApPatFam.Multiline = true;
            this.txtBxApPatFam.Name = "txtBxApPatFam";
            this.txtBxApPatFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxApPatFam.TabIndex = 44;
            // 
            // lblApPatFam
            // 
            this.lblApPatFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblApPatFam.AutoSize = true;
            this.lblApPatFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApPatFam.Location = new System.Drawing.Point(3, 9);
            this.lblApPatFam.Name = "lblApPatFam";
            this.lblApPatFam.Size = new System.Drawing.Size(117, 17);
            this.lblApPatFam.TabIndex = 43;
            this.lblApPatFam.Text = "Apellido Paterno:";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 547F));
            this.tableLayoutPanel17.Controls.Add(this.txtBxApMatFam, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.lblApMatFam, 0, 0);
            this.tableLayoutPanel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel17.Location = new System.Drawing.Point(12, 1151);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel17.TabIndex = 45;
            // 
            // txtBxApMatFam
            // 
            this.txtBxApMatFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxApMatFam.Location = new System.Drawing.Point(124, 3);
            this.txtBxApMatFam.MaxLength = 30;
            this.txtBxApMatFam.Multiline = true;
            this.txtBxApMatFam.Name = "txtBxApMatFam";
            this.txtBxApMatFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxApMatFam.TabIndex = 44;
            // 
            // lblApMatFam
            // 
            this.lblApMatFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblApMatFam.AutoSize = true;
            this.lblApMatFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApMatFam.Location = new System.Drawing.Point(3, 9);
            this.lblApMatFam.Name = "lblApMatFam";
            this.lblApMatFam.Size = new System.Drawing.Size(115, 16);
            this.lblApMatFam.TabIndex = 43;
            this.lblApMatFam.Text = "Apellido Materno:";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 4;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 448F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel18.Controls.Add(this.txtBxNumFam, 3, 0);
            this.tableLayoutPanel18.Controls.Add(this.lblNumFam, 2, 0);
            this.tableLayoutPanel18.Controls.Add(this.txtBxCalleFam, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.lblCalleFam, 0, 0);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(12, 1233);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel18.TabIndex = 46;
            // 
            // txtBxNumFam
            // 
            this.txtBxNumFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxNumFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxNumFam.Location = new System.Drawing.Point(539, 3);
            this.txtBxNumFam.MaxLength = 5;
            this.txtBxNumFam.Multiline = true;
            this.txtBxNumFam.Name = "txtBxNumFam";
            this.txtBxNumFam.Size = new System.Drawing.Size(104, 29);
            this.txtBxNumFam.TabIndex = 47;
            // 
            // lblNumFam
            // 
            this.lblNumFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumFam.AutoSize = true;
            this.lblNumFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumFam.Location = new System.Drawing.Point(503, 9);
            this.lblNumFam.Name = "lblNumFam";
            this.lblNumFam.Size = new System.Drawing.Size(30, 17);
            this.lblNumFam.TabIndex = 47;
            this.lblNumFam.Text = "N°:";
            // 
            // txtBxCalleFam
            // 
            this.txtBxCalleFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxCalleFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxCalleFam.Location = new System.Drawing.Point(55, 3);
            this.txtBxCalleFam.MaxLength = 50;
            this.txtBxCalleFam.Multiline = true;
            this.txtBxCalleFam.Name = "txtBxCalleFam";
            this.txtBxCalleFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxCalleFam.TabIndex = 44;
            // 
            // lblCalleFam
            // 
            this.lblCalleFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCalleFam.AutoSize = true;
            this.lblCalleFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalleFam.Location = new System.Drawing.Point(3, 9);
            this.lblCalleFam.Name = "lblCalleFam";
            this.lblCalleFam.Size = new System.Drawing.Size(46, 17);
            this.lblCalleFam.TabIndex = 43;
            this.lblCalleFam.Text = "Calle:";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.lblDomFam, 0, 0);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(12, 1192);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel19.TabIndex = 47;
            // 
            // lblDomFam
            // 
            this.lblDomFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDomFam.AutoSize = true;
            this.lblDomFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDomFam.Location = new System.Drawing.Point(3, 8);
            this.lblDomFam.Name = "lblDomFam";
            this.lblDomFam.Size = new System.Drawing.Size(662, 18);
            this.lblDomFam.TabIndex = 43;
            this.lblDomFam.Text = "Domicilio Particular";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 533F));
            this.tableLayoutPanel20.Controls.Add(this.txtBxColoniaFam, 1, 0);
            this.tableLayoutPanel20.Controls.Add(this.lblColoniaFam, 0, 0);
            this.tableLayoutPanel20.Location = new System.Drawing.Point(12, 1274);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel20.TabIndex = 48;
            // 
            // txtBxColoniaFam
            // 
            this.txtBxColoniaFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxColoniaFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxColoniaFam.Location = new System.Drawing.Point(138, 3);
            this.txtBxColoniaFam.MaxLength = 50;
            this.txtBxColoniaFam.Multiline = true;
            this.txtBxColoniaFam.Name = "txtBxColoniaFam";
            this.txtBxColoniaFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxColoniaFam.TabIndex = 44;
            // 
            // lblColoniaFam
            // 
            this.lblColoniaFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColoniaFam.AutoSize = true;
            this.lblColoniaFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColoniaFam.Location = new System.Drawing.Point(3, 9);
            this.lblColoniaFam.Name = "lblColoniaFam";
            this.lblColoniaFam.Size = new System.Drawing.Size(129, 17);
            this.lblColoniaFam.TabIndex = 43;
            this.lblColoniaFam.Text = "Colonia/Población:";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 504F));
            this.tableLayoutPanel21.Controls.Add(this.txtBxMunicipioFam, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.lblMunicipioFam, 0, 0);
            this.tableLayoutPanel21.Location = new System.Drawing.Point(12, 1315);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel21.TabIndex = 49;
            // 
            // txtBxMunicipioFam
            // 
            this.txtBxMunicipioFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxMunicipioFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxMunicipioFam.Location = new System.Drawing.Point(167, 3);
            this.txtBxMunicipioFam.MaxLength = 30;
            this.txtBxMunicipioFam.Multiline = true;
            this.txtBxMunicipioFam.Name = "txtBxMunicipioFam";
            this.txtBxMunicipioFam.Size = new System.Drawing.Size(356, 29);
            this.txtBxMunicipioFam.TabIndex = 44;
            // 
            // lblMunicipioFam
            // 
            this.lblMunicipioFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMunicipioFam.AutoSize = true;
            this.lblMunicipioFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMunicipioFam.Location = new System.Drawing.Point(3, 9);
            this.lblMunicipioFam.Name = "lblMunicipioFam";
            this.lblMunicipioFam.Size = new System.Drawing.Size(158, 17);
            this.lblMunicipioFam.TabIndex = 43;
            this.lblMunicipioFam.Text = "Delegación o Municipio:";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 4;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 326F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel22.Controls.Add(this.txtBxCPFam, 3, 0);
            this.tableLayoutPanel22.Controls.Add(this.lblCPFam, 2, 0);
            this.tableLayoutPanel22.Controls.Add(this.txtBxEstadoFam, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.lblEstadoFam, 0, 0);
            this.tableLayoutPanel22.Location = new System.Drawing.Point(12, 1356);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 1;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel22.TabIndex = 50;
            // 
            // txtBxCPFam
            // 
            this.txtBxCPFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxCPFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxCPFam.Location = new System.Drawing.Point(523, 3);
            this.txtBxCPFam.MaxLength = 6;
            this.txtBxCPFam.Multiline = true;
            this.txtBxCPFam.Name = "txtBxCPFam";
            this.txtBxCPFam.Size = new System.Drawing.Size(104, 29);
            this.txtBxCPFam.TabIndex = 51;
            // 
            // lblCPFam
            // 
            this.lblCPFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCPFam.AutoSize = true;
            this.lblCPFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPFam.Location = new System.Drawing.Point(466, 9);
            this.lblCPFam.Name = "lblCPFam";
            this.lblCPFam.Size = new System.Drawing.Size(51, 17);
            this.lblCPFam.TabIndex = 51;
            this.lblCPFam.Text = "C.P.:";
            // 
            // txtBxEstadoFam
            // 
            this.txtBxEstadoFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxEstadoFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxEstadoFam.Location = new System.Drawing.Point(140, 3);
            this.txtBxEstadoFam.MaxLength = 30;
            this.txtBxEstadoFam.Multiline = true;
            this.txtBxEstadoFam.Name = "txtBxEstadoFam";
            this.txtBxEstadoFam.Size = new System.Drawing.Size(320, 29);
            this.txtBxEstadoFam.TabIndex = 44;
            // 
            // lblEstadoFam
            // 
            this.lblEstadoFam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEstadoFam.AutoSize = true;
            this.lblEstadoFam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoFam.Location = new System.Drawing.Point(3, 9);
            this.lblEstadoFam.Name = "lblEstadoFam";
            this.lblEstadoFam.Size = new System.Drawing.Size(131, 17);
            this.lblEstadoFam.TabIndex = 43;
            this.lblEstadoFam.Text = "Entidad Federativa:";
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel23.ColumnCount = 1;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.3454F));
            this.tableLayoutPanel23.Controls.Add(this.lblObser, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.txtBxObser, 0, 1);
            this.tableLayoutPanel23.Location = new System.Drawing.Point(12, 1397);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(652, 116);
            this.tableLayoutPanel23.TabIndex = 51;
            // 
            // lblObser
            // 
            this.lblObser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblObser.AutoSize = true;
            this.lblObser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObser.Location = new System.Drawing.Point(3, 6);
            this.lblObser.Name = "lblObser";
            this.lblObser.Size = new System.Drawing.Size(646, 17);
            this.lblObser.TabIndex = 40;
            this.lblObser.Text = "Observaciones generales acerca del ususuario:";
            // 
            // txtBxObser
            // 
            this.txtBxObser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBxObser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxObser.Location = new System.Drawing.Point(3, 33);
            this.txtBxObser.MaxLength = 400;
            this.txtBxObser.Multiline = true;
            this.txtBxObser.Name = "txtBxObser";
            this.txtBxObser.Size = new System.Drawing.Size(646, 80);
            this.txtBxObser.TabIndex = 41;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 2;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 268F));
            this.tableLayoutPanel24.Controls.Add(this.numUpDnAportación, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.lblPromesa, 0, 0);
            this.tableLayoutPanel24.Location = new System.Drawing.Point(12, 1516);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel24.TabIndex = 52;
            // 
            // numUpDnAportación
            // 
            this.numUpDnAportación.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numUpDnAportación.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUpDnAportación.Location = new System.Drawing.Point(403, 6);
            this.numUpDnAportación.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numUpDnAportación.Name = "numUpDnAportación";
            this.numUpDnAportación.Size = new System.Drawing.Size(120, 22);
            this.numUpDnAportación.TabIndex = 53;
            this.numUpDnAportación.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPromesa
            // 
            this.lblPromesa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPromesa.AutoSize = true;
            this.lblPromesa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPromesa.Location = new System.Drawing.Point(3, 9);
            this.lblPromesa.Name = "lblPromesa";
            this.lblPromesa.Size = new System.Drawing.Size(394, 16);
            this.lblPromesa.TabIndex = 43;
            this.lblPromesa.Text = "Promesa de aportación semanal voluntaria para apoyo:";
            // 
            // bttnGuardar
            // 
            this.bttnGuardar.BackColor = System.Drawing.Color.RoyalBlue;
            this.bttnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnGuardar.ForeColor = System.Drawing.Color.White;
            this.bttnGuardar.Location = new System.Drawing.Point(218, 1621);
            this.bttnGuardar.Name = "bttnGuardar";
            this.bttnGuardar.Size = new System.Drawing.Size(100, 50);
            this.bttnGuardar.TabIndex = 53;
            this.bttnGuardar.Text = "Guardar";
            this.bttnGuardar.UseVisualStyleBackColor = false;
            this.bttnGuardar.Click += new System.EventHandler(this.bttnGuardar_Click);
            this.bttnGuardar.MouseEnter += new System.EventHandler(this.mouseGuardarEnter);
            this.bttnGuardar.MouseLeave += new System.EventHandler(this.mouseGuardarLeave);
            // 
            // bttnCancelar
            // 
            this.bttnCancelar.BackColor = System.Drawing.Color.Orange;
            this.bttnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnCancelar.ForeColor = System.Drawing.Color.Black;
            this.bttnCancelar.Location = new System.Drawing.Point(368, 1621);
            this.bttnCancelar.Name = "bttnCancelar";
            this.bttnCancelar.Size = new System.Drawing.Size(100, 50);
            this.bttnCancelar.TabIndex = 54;
            this.bttnCancelar.Text = "Cancelar";
            this.bttnCancelar.UseVisualStyleBackColor = false;
            this.bttnCancelar.Click += new System.EventHandler(this.clickCancelar);
            this.bttnCancelar.MouseEnter += new System.EventHandler(this.mouseCancelarEnter);
            this.bttnCancelar.MouseLeave += new System.EventHandler(this.mouseCancelarLeave);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 326F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel6.Controls.Add(this.txtBxCP, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.lblCP, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtBxEntFederativa, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.lblEntFederativa, 0, 0);
            this.tableLayoutPanel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel6.Location = new System.Drawing.Point(12, 399);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel6.TabIndex = 59;
            // 
            // txtBxCP
            // 
            this.txtBxCP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxCP.Location = new System.Drawing.Point(523, 3);
            this.txtBxCP.MaxLength = 6;
            this.txtBxCP.Multiline = true;
            this.txtBxCP.Name = "txtBxCP";
            this.txtBxCP.Size = new System.Drawing.Size(104, 29);
            this.txtBxCP.TabIndex = 51;
            // 
            // lblCP
            // 
            this.lblCP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCP.AutoSize = true;
            this.lblCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCP.Location = new System.Drawing.Point(466, 9);
            this.lblCP.Name = "lblCP";
            this.lblCP.Size = new System.Drawing.Size(51, 17);
            this.lblCP.TabIndex = 51;
            this.lblCP.Text = "C.P.:";
            // 
            // txtBxEntFederativa
            // 
            this.txtBxEntFederativa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxEntFederativa.Location = new System.Drawing.Point(140, 3);
            this.txtBxEntFederativa.MaxLength = 30;
            this.txtBxEntFederativa.Multiline = true;
            this.txtBxEntFederativa.Name = "txtBxEntFederativa";
            this.txtBxEntFederativa.Size = new System.Drawing.Size(320, 29);
            this.txtBxEntFederativa.TabIndex = 44;
            // 
            // lblEntFederativa
            // 
            this.lblEntFederativa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntFederativa.AutoSize = true;
            this.lblEntFederativa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntFederativa.Location = new System.Drawing.Point(3, 9);
            this.lblEntFederativa.Name = "lblEntFederativa";
            this.lblEntFederativa.Size = new System.Drawing.Size(131, 17);
            this.lblEntFederativa.TabIndex = 43;
            this.lblEntFederativa.Text = "Entidad Federativa:";
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 2;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 504F));
            this.tableLayoutPanel25.Controls.Add(this.txtBxMunicipio, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.lblMunicipio, 0, 0);
            this.tableLayoutPanel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel25.Location = new System.Drawing.Point(12, 358);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 1;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel25.TabIndex = 58;
            // 
            // txtBxMunicipio
            // 
            this.txtBxMunicipio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxMunicipio.Location = new System.Drawing.Point(167, 3);
            this.txtBxMunicipio.MaxLength = 30;
            this.txtBxMunicipio.Multiline = true;
            this.txtBxMunicipio.Name = "txtBxMunicipio";
            this.txtBxMunicipio.Size = new System.Drawing.Size(356, 29);
            this.txtBxMunicipio.TabIndex = 44;
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMunicipio.AutoSize = true;
            this.lblMunicipio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMunicipio.Location = new System.Drawing.Point(3, 9);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(158, 17);
            this.lblMunicipio.TabIndex = 43;
            this.lblMunicipio.Text = "Delegación o Municipio:";
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 2;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 533F));
            this.tableLayoutPanel26.Controls.Add(this.txtBxColonia, 1, 0);
            this.tableLayoutPanel26.Controls.Add(this.lblColonia, 0, 0);
            this.tableLayoutPanel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel26.Location = new System.Drawing.Point(12, 317);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 1;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel26.TabIndex = 57;
            // 
            // txtBxColonia
            // 
            this.txtBxColonia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxColonia.Location = new System.Drawing.Point(138, 3);
            this.txtBxColonia.MaxLength = 50;
            this.txtBxColonia.Multiline = true;
            this.txtBxColonia.Name = "txtBxColonia";
            this.txtBxColonia.Size = new System.Drawing.Size(356, 29);
            this.txtBxColonia.TabIndex = 44;
            // 
            // lblColonia
            // 
            this.lblColonia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColonia.AutoSize = true;
            this.lblColonia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColonia.Location = new System.Drawing.Point(3, 9);
            this.lblColonia.Name = "lblColonia";
            this.lblColonia.Size = new System.Drawing.Size(129, 17);
            this.lblColonia.TabIndex = 43;
            this.lblColonia.Text = "Colonia/Población:";
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 1;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Controls.Add(this.lblDomPart, 0, 0);
            this.tableLayoutPanel27.Location = new System.Drawing.Point(12, 235);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel27.TabIndex = 56;
            // 
            // lblDomPart
            // 
            this.lblDomPart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDomPart.AutoSize = true;
            this.lblDomPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDomPart.Location = new System.Drawing.Point(3, 8);
            this.lblDomPart.Name = "lblDomPart";
            this.lblDomPart.Size = new System.Drawing.Size(662, 18);
            this.lblDomPart.TabIndex = 43;
            this.lblDomPart.Text = "Domicilio Particular";
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 4;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 448F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel28.Controls.Add(this.txtBxNum, 3, 0);
            this.tableLayoutPanel28.Controls.Add(this.lblNum, 2, 0);
            this.tableLayoutPanel28.Controls.Add(this.txtBxCalle, 1, 0);
            this.tableLayoutPanel28.Controls.Add(this.lblCalle, 0, 0);
            this.tableLayoutPanel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel28.Location = new System.Drawing.Point(12, 276);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 1;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(668, 35);
            this.tableLayoutPanel28.TabIndex = 55;
            // 
            // txtBxNum
            // 
            this.txtBxNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxNum.Location = new System.Drawing.Point(539, 3);
            this.txtBxNum.MaxLength = 5;
            this.txtBxNum.Multiline = true;
            this.txtBxNum.Name = "txtBxNum";
            this.txtBxNum.Size = new System.Drawing.Size(104, 29);
            this.txtBxNum.TabIndex = 47;
            // 
            // lblNum
            // 
            this.lblNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNum.AutoSize = true;
            this.lblNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNum.Location = new System.Drawing.Point(503, 9);
            this.lblNum.Name = "lblNum";
            this.lblNum.Size = new System.Drawing.Size(30, 17);
            this.lblNum.TabIndex = 47;
            this.lblNum.Text = "N°:";
            // 
            // txtBxCalle
            // 
            this.txtBxCalle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBxCalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxCalle.Location = new System.Drawing.Point(55, 3);
            this.txtBxCalle.MaxLength = 50;
            this.txtBxCalle.Multiline = true;
            this.txtBxCalle.Name = "txtBxCalle";
            this.txtBxCalle.Size = new System.Drawing.Size(356, 29);
            this.txtBxCalle.TabIndex = 44;
            // 
            // lblCalle
            // 
            this.lblCalle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCalle.AutoSize = true;
            this.lblCalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalle.Location = new System.Drawing.Point(3, 9);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(46, 17);
            this.lblCalle.TabIndex = 43;
            this.lblCalle.Text = "Calle:";
            // 
            // frmAgregarInterno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(10, 150);
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(696, 531);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Controls.Add(this.tableLayoutPanel25);
            this.Controls.Add(this.tableLayoutPanel26);
            this.Controls.Add(this.tableLayoutPanel27);
            this.Controls.Add(this.tableLayoutPanel28);
            this.Controls.Add(this.bttnCancelar);
            this.Controls.Add(this.bttnGuardar);
            this.Controls.Add(this.tableLayoutPanel24);
            this.Controls.Add(this.tableLayoutPanel23);
            this.Controls.Add(this.tableLayoutPanel22);
            this.Controls.Add(this.tableLayoutPanel21);
            this.Controls.Add(this.tableLayoutPanel20);
            this.Controls.Add(this.tableLayoutPanel19);
            this.Controls.Add(this.tableLayoutPanel18);
            this.Controls.Add(this.tableLayoutPanel17);
            this.Controls.Add(this.tableLayoutPanel16);
            this.Controls.Add(this.tableLayoutPanel15);
            this.Controls.Add(this.tableLayoutPanel14);
            this.Controls.Add(this.tableLayoutPanel13);
            this.Controls.Add(this.tableLayoutPanel12);
            this.Controls.Add(this.tableLayoutPanel11);
            this.Controls.Add(this.tableLayoutPanel10);
            this.Controls.Add(this.tableLayoutPanel9);
            this.Controls.Add(this.tableLayoutPanel8);
            this.Controls.Add(this.tableLayoutPanel7);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "frmAgregarInterno";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar un interno";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AgregarInterno_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnEdad)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnAportación)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel27.PerformLayout();
            this.tableLayoutPanel28.ResumeLayout(false);
            this.tableLayoutPanel28.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBxNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.NumericUpDown numUpDnEdad;
        private System.Windows.Forms.Label lblApPaterno;
        private System.Windows.Forms.ComboBox cmbBxSexo;
        private System.Windows.Forms.TextBox txtBxApPaterno;
        private System.Windows.Forms.DateTimePicker dtTmFechaNac;
        private System.Windows.Forms.Label lblFechaNac;
        private System.Windows.Forms.Label lblApMaterno;
        private System.Windows.Forms.TextBox txtBxApMaterno;
        private System.Windows.Forms.ComboBox cmbBxEstadoCivil;
        private System.Windows.Forms.ComboBox cmbBxEscolaridad;
        private System.Windows.Forms.Label lblOcupacion;
        private System.Windows.Forms.TextBox txtBxOcupacion;
        private System.Windows.Forms.Label lblServMedico;
        private System.Windows.Forms.TextBox txtBxServMedico;
        private System.Windows.Forms.Label lblNSS;
        private System.Windows.Forms.TextBox txtBxNSS;
        private System.Windows.Forms.ComboBox cmbBxIngreso;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.TextBox txtBxProviene;
        private System.Windows.Forms.ComboBox cmbBxAcude;
        private System.Windows.Forms.TextBox txtBxParentesco;
        private System.Windows.Forms.Label lblParentesco;
        private System.Windows.Forms.Label lblProviene;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TextBox txtBxRelacion;
        private System.Windows.Forms.Label lblRelacion;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label lblRevision;
        private System.Windows.Forms.TextBox txtBxRevision;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.CheckedListBox chckListEnfermedad;
        private System.Windows.Forms.Label lblEnfermedad;
        private System.Windows.Forms.TextBox txtBxEnfermedades;
        private System.Windows.Forms.CheckedListBox chckListMedicamento;
        private System.Windows.Forms.Label lblMedicamento;
        private System.Windows.Forms.TextBox txtBxMedicamento;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.TextBox txtBxDrogasIng;
        private System.Windows.Forms.Label lblDrogasIng;
        private System.Windows.Forms.CheckedListBox chckListDrogasCons;
        private System.Windows.Forms.Label lblDrogasCons;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label lblDatosFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TextBox txtBxNombreFam;
        private System.Windows.Forms.Label lblNombreFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TextBox txtBxApPatFam;
        private System.Windows.Forms.Label lblApPatFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.TextBox txtBxApMatFam;
        private System.Windows.Forms.Label lblApMatFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.TextBox txtBxNumFam;
        private System.Windows.Forms.Label lblNumFam;
        private System.Windows.Forms.TextBox txtBxCalleFam;
        private System.Windows.Forms.Label lblCalleFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Label lblDomFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.TextBox txtBxColoniaFam;
        private System.Windows.Forms.Label lblColoniaFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.TextBox txtBxMunicipioFam;
        private System.Windows.Forms.Label lblMunicipioFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.TextBox txtBxCPFam;
        private System.Windows.Forms.Label lblCPFam;
        private System.Windows.Forms.TextBox txtBxEstadoFam;
        private System.Windows.Forms.Label lblEstadoFam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Label lblObser;
        private System.Windows.Forms.TextBox txtBxObser;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.NumericUpDown numUpDnAportación;
        private System.Windows.Forms.Label lblPromesa;
        private System.Windows.Forms.Button bttnGuardar;
        private System.Windows.Forms.Button bttnCancelar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox txtBxCP;
        private System.Windows.Forms.Label lblCP;
        private System.Windows.Forms.TextBox txtBxEntFederativa;
        private System.Windows.Forms.Label lblEntFederativa;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.TextBox txtBxMunicipio;
        private System.Windows.Forms.Label lblMunicipio;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.TextBox txtBxColonia;
        private System.Windows.Forms.Label lblColonia;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.Label lblDomPart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.TextBox txtBxNum;
        private System.Windows.Forms.Label lblNum;
        private System.Windows.Forms.TextBox txtBxCalle;
        private System.Windows.Forms.Label lblCalle;
    }
}