﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroInterno
{
    public partial class frmEgresos : Form
    {
        string strProcedimiento = null;
        int indiceTabla = 0;
        string id = string.Empty;

        public frmEgresos()
        {
            InitializeComponent();
        }

        public frmEgresos(string c_strProcedimiento, int c_indiceTabla)
        {
            InitializeComponent();
            strProcedimiento = c_strProcedimiento;
            indiceTabla = c_indiceTabla;
        }

        private void frmEgresos_Load(object sender, EventArgs e)
        {
            dtgridLoad();
        }

        void dtgridLoad()
        {
            conexion conn = new conexion();
            dtGridLista.DataSource = conn.Llenartabla(strProcedimiento, "listaegresos");
        }

        private void mouseAnadirLeave(object sender, EventArgs e)
        {
            btnAnadir.BackColor = Color.FromArgb(106, 168, 79);
        }

        private void mouseAnadirEnter(object sender, EventArgs e)
        {
            btnAnadir.BackColor = Color.FromArgb(165, 216, 143);
        }

        private void mouseEliminarEnter(object sender, EventArgs e)
        {
            btnEliminar.BackColor = Color.FromArgb(237, 160, 160);
        }

        private void mouseEliminarLeave(object sender, EventArgs e)
        {
            btnEliminar.BackColor = Color.FromArgb(224, 102, 102);
        }
        
        private void btnAnadir_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmNuevoEgreso egreso = new frmNuevoEgreso();
            egreso.Show();
        }

        private void clickMenu(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }

        private void dtGridLista_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dtGridLista.Rows[e.RowIndex].Cells[0].Value != null)
            {
                id = dtGridLista.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }

        private void frmEgresos_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            dtGridLista.Rows.Remove(dtGridLista.CurrentRow);
        }
    }
}
