﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroInterno
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void mouseEnterBen(object sender, EventArgs e)
        {
            btnEgresos.BackColor = Color.CornflowerBlue;
        }

        private void mouseLeaveBen(object sender, EventArgs e)
        {
            btnEgresos.BackColor = Color.RoyalBlue;
        }

        private void mouseEnterInt(object sender, EventArgs e)
        {
            btnInternos.BackColor = Color.CornflowerBlue;
        }

        private void mouseLeaveInt(object sender, EventArgs e)
        {
            btnInternos.BackColor = Color.RoyalBlue;
        }

        private void clickCambiarContrasena(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            frmCambiarContrasena cambiar = new frmCambiarContrasena();
            cambiar.Show();
        }

        private void frmMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void clickInternos(object sender, EventArgs e)
        {
            this.Hide();
            frmInternos internos = new frmInternos("mostrarInternos",3);
            internos.Show();
        }

        private void btnBendiciones_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmEgresos egresos = new frmEgresos("mostrarEgresos", 4);
            egresos.Show();
        }
    }
}
